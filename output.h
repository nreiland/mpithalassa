// MODULES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

/* external functions */
extern void cart2coe4c_(double R[], double V[], double COE[], double *GM);

/* function to initialize output folder */
int initDirOut(char dirOut[], int numIntervals, int rank)
{
    // function definitions
    int cleanDir(char dirPath[]);    

    if (rank == 0)
    {
        int i, j, k;
        char buf[1000];
        char traj[1000], states[1000], cols[1000], interval[1000], origTraj[1000], origTargTraj[1000], origFieldTraj[1000];
        char targTraj[1000], fieldTraj[1000], targStates[1000], fieldStates[1000];
        char fieldTraj1[1000], targTraj1[1000], fieldState1[1000], targState1[1000], col1[1000];
        char fieldTraj2[1000], targTraj2[1000], fieldState2[1000], targState2[1000], col2[1000];
        char fieldTraj3[1000], targTraj3[1000], fieldState3[1000], targState3[1000], col3[1000];

        // make output directory
        cleanDir(dirOut);

        // make sub folders for each time interval
        for (i = 0; i < numIntervals; i++)
        {
            printf("%d\n",i);
            // make strings for sub folders
            snprintf(interval, 10, "%d", i);
            // itoa(i, interval, 10);

            // states
            strcpy(states, dirOut);
            strcat(states, "/");
            strcat(states, interval);
            mkdir(states, 0777);
            strcat(states, "/states/");
            mkdir(states, 0777);


            // trajectories
            strcpy(traj, dirOut);
            strcat(traj, "/");
            strcat(traj, interval);
            strcat(traj, "/trajectories/");
            mkdir(traj, 0777);

        }
    }

    return 1; 
}

/* function to print filter output */
int printOutput(double **objectTrajectory, double **objectStates, char dirOut[], int rank, int *objectTrajLengths, int numObjs, int *exitCodes, int idx)
{
    // function definitions
    int outputTrajectory(char dir[], double **traj, int numObjs, int *trajLength, int *exitCodes);
    int outputState(char dir[], double **states, int numObjs, int *exitCodes);


    if (rank == 0)
    {

        int i, j, k;
        char traj[1000], states[1000], interval[1000];
        char objectTraj[1000], objectState[1000];
    

        // define path to files
        snprintf(interval, 10, "%d", idx);
        // itoa(i, interval, 10);

        // trajectories
        strcpy(traj, dirOut);
        strcat(traj, "/");
        strcat(traj, interval);
        strcat(traj, "/trajectories");
        mkdir(traj, 0777);

        // states
        strcpy(states, dirOut);
        strcat(states, "/");
        strcat(states, interval);
        strcat(states, "/states");
        mkdir(states, 0777);

        // print
        outputTrajectory(traj, objectTrajectory, numObjs, objectTrajLengths, exitCodes);
        outputState(states, objectStates, numObjs, exitCodes);
         
        return 0;

    }
        
    else
    {
        return 0;
    }
    
    
}

/* function to recursively delete a directory*/
int remove_directory(const char *path)
{
    DIR *d = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;
    
    if (d)
    {
        struct dirent *p;
        
        r = 0;
        
        while (!r && (p=readdir(d)))
        {
            int r2 = -1;
            char *buf;
            size_t len;
            
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
            {
                continue;
            }
            
            len = path_len + strlen(p->d_name) + 2;
            buf = malloc(len);
            
            if (buf)
            {
                struct stat statbuf;
                
                snprintf(buf, len, "%s/%s", path, p->d_name);
                
                if (!stat(buf, &statbuf))
                {
                    if (S_ISDIR(statbuf.st_mode))
                    {
                        r2 = remove_directory(buf);
                    }
                    else
                    {
                        r2 = unlink(buf);
                    }
                }
                
                free(buf);
            }
            
            r = r2;
        }
        
        closedir(d);
    }
    
    if (!r)
    {
        r = rmdir(path);
    }
    
    return r;
}

/* function to delete directory if it exists and then create new directory */
int cleanDir(char dirPath[])
{
    // functions
    int remove_directory(const char *path);

    const char *dirPoint = dirPath;
    
    DIR* dir = opendir(dirPath);
    if (dir)
    {
        /* Directory exists. */
        closedir(dir);
        remove_directory(dirPoint);
        mkdir(dirPath, 0777);
    }
    else if (ENOENT == errno)
    {
        /* Directory does not exist. */
        mkdir(dirPath, 0777);
        
    }
    else
    {
        /* opendir() failed for some other reason. */
        printf("opendir() failed for some other reason");
        exit(-1);
    }
    
    return 0;
}

/* function to print trajectories */
int outputTrajectory(char dir[], double **traj, int numObjs, int *trajLength, int *exitCodes)
{
    int i, j, k;
    int npts;
    int objIdx;
    int localIdx;
    int printAll = 1;
    char fname[1000];
    char fname2[1000];
    char idx[1000];
    double mu = 3.986004414498200E+05;
    double COE[6];
    double orbs[6];
    double R[3];
    double V[3];
    double r2d = 180/M_PI;

    FILE *file;

    // print trajectories
    for (j = 0; j < numObjs; j++)
    {
        // get global object index
        printf("print %d of %d\n",j,numObjs);
        objIdx = j;

        // if object is active or inactive
        if(exitCodes[objIdx] == 0 || printAll == 1)
        {

            // get number of points
            npts = trajLength[objIdx];
                
            snprintf(idx, 10, "%d", objIdx);
            strcpy(fname, dir);
            strcat(fname, "/");
            strcat(fname, idx);
            strcat(fname, ".txt");

            file = fopen(fname, "w");

            if (file == NULL)
            {
                printf("Error opening file!\n");
                exit(-1);
            }

            for (i = 0; i < npts; i++)
            {
                // convert to orbital elements
                R[0] = traj[objIdx][i * 12 + 1];
                R[1] = traj[objIdx][i * 12 + 2];
                R[2] = traj[objIdx][i * 12 + 3];
                V[0] = traj[objIdx][i * 12 + 4];
                V[1] = traj[objIdx][i * 12 + 5];
                V[2] = traj[objIdx][i * 12 + 6];

                // call fortran conversion routine
                cart2coe4c_(R, V, &COE[0], &mu);

                //update states
                // objectStates[objIdx][0] = objectTraj->arr[finalIdx * 12 + 0];
                orbs[0] = COE[0];
                orbs[1] = COE[1];
                orbs[2] = COE[2]*r2d;
                orbs[3] = COE[3]*r2d;
                orbs[4] = COE[4]*r2d;
                orbs[5] = COE[5]*r2d;
                
                fprintf(file,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", traj[objIdx][i * 12 + 0],
                    orbs[0], orbs[1], orbs[2], orbs[3],
                    orbs[4], orbs[5], traj[objIdx][i * 12 + 7],traj[objIdx][i * 12 + 8],
                    traj[objIdx][i * 12 + 9], traj[objIdx][i * 12 + 10],traj[objIdx][i * 12 + 11]);
            }

            fclose(file);

        }
    }

    return 0;

}

/* function to print states together */
int outputState(char dir[], double **states, int numObjs, int *exitCodes)
{
    int i, j, k;
    int npts;
    int objIdx;
    int localIdx;
    char fname[1000];
    char fname2[1000];
    char idx[1000];

    FILE *file;

    // define file name
    strcpy(fname, dir);
    strcat(fname, "/");
    strcat(fname, "states");
    strcat(fname, ".txt");

    // open file
    file = fopen(fname, "w");

    // exit if file is null
    if (file == NULL)
    {
        printf("Error opening file!\n");
        exit(-1);
    }

    // print states
    for (j = 0; j < numObjs; j++)
    {
        // get global object index
        objIdx = j;

        // if object is active
        if (exitCodes[objIdx] == 0)
        {
            
            fprintf(file, "%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n",
                    states[objIdx][0], states[objIdx][1], states[objIdx][2], states[objIdx][3],
                    states[objIdx][4], states[objIdx][5], states[objIdx][6], states[objIdx][7],
                    states[objIdx][8], states[objIdx][9], states[objIdx][10], states[objIdx][11]);

        }
    }

    // close file
    fclose(file);

    return 0;

}


/* function to print states seperately */
int outputStateSep(char dir[], double **states, int numObjs, int *exitCodes)
{
    int i, j, k;
    int npts;
    int objIdx;
    int localIdx;
    char fname[1000];
    char fname2[1000];
    char idx[1000];

    FILE *file;

    // print states
    for (j = 0; j < numObjs; j++)
    {
        // get global object index
        objIdx = j;

        // if object is active
        if (exitCodes[objIdx] == 0)
        {

            snprintf(idx, 10, "%d", objIdx);
            strcpy(fname, dir);
            strcat(fname, "/");
            strcat(fname, idx);
            strcat(fname, ".txt");

            file = fopen(fname, "w");

            if (file == NULL)
            {
                printf("Error opening file!\n");
                exit(-1);
            }

            fprintf(file, "%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    states[objIdx][0], states[objIdx][1], states[objIdx][2], states[objIdx][3],
                    states[objIdx][4], states[objIdx][5], states[objIdx][6], states[objIdx][7],
                    states[objIdx][8], states[objIdx][9], states[objIdx][10], states[objIdx][11]);

            fclose(file);

        }
    }

    return 0;

}
