#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

// #include "contigDouble.h"

/*function definitions*/
extern void thalassasub_( double *MJD0, double *COE0, double *tspan, double *tstep, int *insgrav, int *isun, int *imoon, 
    int *idrag, int *iF107, int *iSRP, int *iephem, int *gdeg, int *gord, int *rmxstep, double *tol, int *imcoll,
    int *eqs, double *SCMass, double *ADrag, double *ASRP, double *CD, double *CR, int *mxpts, int *npts, double cart[], int *tag,
    int *exitcode  );


/* function to propagate target objects */
int propObjs(int rank, struct jobParams jobs, struct Input input, double **objectTraj, double **objectStates,
                int *mxpts, double *partialTspan, double tstep, double mu, int *trajLengths, int numJobs, int *objectPropJobs,
                int *exitCodes, int lenSet, double *mem, int bugtag, int cpus)
{
    if ( bugtag == 0 )
        printf("starting propagation - on core: %d\n",rank);
    // need pointers to what trajectories and states are held by what processor - master will be in charge of this
    // define variables
    int i, j, k, l, idx, njobs, tag, tag2, npts;
    int orbelType, count, objIdx, jobIdx, start;
    int exitcode;
    int exitCodeBuff[jobs.rootNumPropJobs + 1];
    int locTrajIdx;
    int propJobs[jobs.rootNumPropJobs + 1];
    int cpuIdx;
    int maxpoints;
    int exitCode;
    int locNumJobs;
    double COE0[6];

    // define MPI Status
    MPI_Status stat;

    // get max ammount of mxpts
    // maxMxpts = 0;
    // for(i = 0; i < lenSet; i++)
    // {
    //     if (mxpts[i] > maxMxpts)
    //         maxMxpts = mxpts[i];

    // }

    // create contiguous derived data type for propagation jobs
    // MPI_Datatype trajSend;
    MPI_Datatype ecodeArr;
    // MPI_Type_contiguous(12*maxMxpts, MPI_DOUBLE, &trajSend);
    MPI_Type_contiguous(jobs.rootNumPropJobs + 1, MPI_INT, &ecodeArr);
    // MPI_Type_commit(&trajSend);
    MPI_Type_commit(&ecodeArr);

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < numJobs; i++)
    {
        // assign
        if(cpuIdx == rank)
        {
            propJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if it exceeds number of cpus
        if(cpuIdx == cpus)
            cpuIdx = 1;

    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;


    // MASTER PROCESSOR no longer has jobs - administrator
    if (rank == 0)
    {
        // collect trajectories
        int runFlag = 1;
        int completedJobs = 0;
        while(runFlag == 1)
        {
            // get object index
            // printf("scanning on root...\n");
            MPI_Recv(&objIdx, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);

            // get trajectory for object, tag - 1
            tag = objIdx;
            // printf("looking for npts of object: %d\n", tag);
            MPI_Recv(&npts, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("got npts(%d) of %d\n", npts,tag);
            MPI_Recv(&exitCode, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("looking for trajectory of object: %d\n", tag);
            MPI_Recv(&maxpoints, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("got maxpoints of %d\n", tag);
            MPI_Recv(objectTraj[objIdx], 12*npts, MPI_DOUBLE, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("got traj of %d\n", tag);
            trajLengths[objIdx] = npts;
            // printf("saved lengths of %d\n", tag);
            exitCodes[objIdx] = exitCode;
            // printf("saved exit codes of %d\n", tag);
            mxpts[objIdx] = maxpoints;
            // printf("saved mxpts of %d\n", tag);
            // printf("COMPLETED OBJ: %d\n",tag);

            // update number of jobs completed
            completedJobs++;

            // exit if all jobs have been completed
            if(completedJobs == numJobs)
                runFlag = 0; 

        }
        // printf("done collecting trajectories\n");

    }

    // SLAVE PROCESS
    else
    {
        
        // call thalassa subroutine and propagate
        double MJD0;
        int npts, propStateIdx;
        double SCMass, ADrag, ASRP, CD, CR;

        for (i = 0; i < locNumJobs; i++)
        {

            // get propagation state
            jobIdx = propJobs[i];
            objIdx = objectPropJobs[jobIdx];

            // define object properties (to be placed in for-loop)
            MJD0 = objectStates[objIdx][0];
            SCMass = objectStates[objIdx][7];
            ADrag = objectStates[objIdx][8];
            ASRP = objectStates[objIdx][9];
            CD = objectStates[objIdx][10];
            CR = objectStates[objIdx][11];
            COE0[0] = objectStates[objIdx][1];
            COE0[1] = objectStates[objIdx][2];
            COE0[2] = objectStates[objIdx][3];
            COE0[3] = objectStates[objIdx][4];
            COE0[4] = objectStates[objIdx][5];
            COE0[5] = objectStates[objIdx][6];

            tag2 = objIdx;

            // set tstep
            if (partialTspan[objIdx] < 0)
            {
                if (tstep > 0)
                {
                    tstep = tstep*-1;
                }
            }

            // DEBUG
            printf("propagating %d on core %d\n", objIdx, rank);
            double clockStart = clock();
            thalassasub_( &MJD0, &COE0[0], &partialTspan[objIdx], &tstep, &input.insgrav, &input.isun, &input.imoon,
            &input.idrag, &input.iF107, &input.iSRP, &input.iephem, &input.gdeg, &input.gord, &mxpts[objIdx], 
            &input.tol, &input.imcoll, &input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &mxpts[objIdx], &npts, 
            objectTraj[0], &tag2, &exitcode);

            // check if mxpts was reached
            while (exitcode == -3)
            {
                // update max number of points
                mxpts[objIdx] += 50000;

                // repropagate
                thalassasub_( &MJD0, &COE0[0], &partialTspan[objIdx], &tstep, &input.insgrav, &input.isun, &input.imoon,
                &input.idrag, &input.iF107, &input.iSRP, &input.iephem, &input.gdeg, &input.gord, &mxpts[objIdx], 
                &input.tol, &input.imcoll, &input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &mxpts[objIdx], &npts, 
                objectTraj[0], &tag2, &exitcode);
            }

            // save maxpoints locally
            maxpoints = mxpts[objIdx];

            double clockEnd = clock();
            double timeUsed = (clockEnd - clockStart)/CLOCKS_PER_SEC;
            printf("%f on job %d on core %d\n",timeUsed, i,rank);

            // DEBUG
            if (bugtag == 0)
                printf("successfully retrieved trajectory %d on core %d\n", objIdx, rank);
            
            // update trajectory length
            trajLengths[objIdx] = npts;

            // fill trajectory with physical parameters
            for (j = 0; j < npts; j++)
            {
                objectTraj[0][j * 12 + 7] = SCMass;
                objectTraj[0][j * 12 + 8] = ADrag;
                objectTraj[0][j * 12 + 9] = ASRP;
                objectTraj[0][j * 12 + 10] = CD;
                objectTraj[0][j * 12 + 11] = CR;

            }

            // define exit code
            if (exitcode != 0)
            {
                printf("prop failure on target object %d with exit code %d detected\n",objIdx,exitcode);
                exitCode = 1;
            }
            else
                exitCode = 0;

            // DEBUG
            // printf("%.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", objectTraj[0][0 * 12 + 0],
            //     objectTraj[0][0 * 12 + 1], objectTraj[0][0 * 12 + 2], objectTraj[0][0 * 12 + 3], objectTraj[0][0 * 12 + 4],
            //     objectTraj[0][0 * 12 + 5], objectTraj[0][0 * 12 + 7], objectTraj[0][0 * 12 + 7],objectTraj[0][i * 12 + 8],
            //     objectTraj[0][0 * 12 + 9], objectTraj[0][0 * 12 + 10],objectTraj[0][0 * 12 + 11]);

            // send trajectory
            tag = objIdx;
            // printf("sending traj: %d - job: %d from cpu: %d\n", tag, jobIdx, rank);
            MPI_Send(&objIdx, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&npts, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&exitCode, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&maxpoints, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            // MPI_Send(objectTraj[0], 1, trajSend, 0, tag, MPI_COMM_WORLD);
            printf("saved trajectory %d on core %d\n", objIdx, rank);
            MPI_Send(objectTraj[0], 12*npts, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
             
        }

        

    }

    // printf("done with propagation\n");

     // synchronize processors
    MPI_Barrier(MPI_COMM_WORLD);
    // printf("done with propagation on core %d\n",rank);

    // send all other trajectory lengths
    MPI_Bcast(trajLengths, lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // send all other processors exit codes
    MPI_Bcast(exitCodes, lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // send all other processors max points
    MPI_Bcast(mxpts, lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // deallocate MPI derived type
    // MPI_Type_free(&trajSend);
    MPI_Type_free(&ecodeArr);

    return 0;

}

