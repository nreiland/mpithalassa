// header file for fica memory management

struct rowInt
{
    int *rows;
    int *columns;
    int **arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 2d semi contiguous array structure*/
 void allocRowInt(struct rowInt *array, int newrows, int newcolumns) {

    int i;
    double buff;

    // allocate mem for organizational pointers
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double));

    // define pointer values for organization pointers
    *(array->rows) = newrows;
    *(array->columns) = newcolumns;

    // allocate memory for main array
    array->arr = (int **)malloc(newrows * sizeof(int *));
    for (i = 0; i < newrows; i++) {
        array->arr[i] = (int *)malloc(newcolumns*sizeof(int));
        memset(array->arr[i], 0, newcolumns*sizeof(int));
    }

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += newrows*sizeof(int *);
    *(array->mem) += sizeof(int **);
    *(array->mem) += newrows*newcolumns*sizeof(int);
    *(array->mem) = *(array->mem)/1e6;
    *(array->prevmem) = 0;

}

int reallocRowInt(struct rowInt *array, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevrows = *(array->rows);
    double buff;

    // define factor of safety
    double fs = 1.0;
    if ( newrows > prevrows )
        newrows = fs*newrows;

    // flag = 1: new rows, flag = 2: prevrows
    int flag = 0; 
    
    // reallocate double pointer if number of rows has increased -> use newrows
    if ( newrows > prevrows ) {

        // printf(" reallocating memory address: %p\n", array->arr );
        // printf("prevrows = %d - - -  newrows = %d\n", prevrows , newrows );

        // reallocate double pointer
        array->arr = realloc( array->arr, newrows*sizeof(int *) );

        // reallocate old rows
        // for (i = 0; i < prevrows; i++) {
        //     array->arr[i] = realloc( array->arr[i], columns*sizeof(int) );
        //     //memset( array->arr[i], 0, columns*sizeof(int) );
        // }
        // allocate new rows
        for (i = prevrows; i < newrows; i++) {
        array->arr[i] = (int *)malloc( columns*sizeof(int) );
            //memset( array->arr[i], 0, columns*sizeof(int) );
        }

        flag = 1;
    }
    // if rows have not increased check if number of rows has increased -> use prevl
    else {

        // // set values to zeros
        // for (i = 0; i < newrows; i++) {
        //     //memset( array->arr[i], 0, columns*sizeof(int) );
        // }

        flag = 2;

    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newrows*sizeof(int *);
        newmem += sizeof(int **);
        newmem += newrows*columns*sizeof(int);

        *(array->rows) = newrows;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevrows*sizeof(int *);
        newmem += sizeof(int **);
        newmem += prevrows*columns*sizeof(int);

        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem/1e6;

    // return int
    return 0;

}
   