#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

// function to update states of objects and propagation, and trajectory jobs and to initialize C
extern void cart2coe4c_(double R[], double V[], double COE[], double *GM);

/* function to update exogenous values for next time chunk*/
int updateStates(double **objectTrajectory, double **objectStates, int *objectTrajLengths, int *exitCodes, 
                    int numObjs, int *objectPropJobs, int rank, int *numJobs, double *totalMem, double *objectPropJobsMem)
{
    // VARS
    int i, j, k, idx, finalState, finalIdx;
    double COE[6], R[3], V[3];
    double mu = 3.986004414498200E+05;
    int numobjectPropJobs, objIdx;
    int targIdx, fieldIdx, exitFlag;
    int tag;
    double r2d = 180/M_PI;
    int buggout = 0;

    // initialize MPI
    MPI_Status stat;
    MPI_Datatype stateSend;
    MPI_Type_contiguous(12, MPI_DOUBLE, &stateSend);
    MPI_Type_commit(&stateSend);

    // calculate number of target propagation jobs required
    numobjectPropJobs = 0;
    for ( i= 0; i < numObjs; i++)
    {
        if (exitCodes[i] == 0)
            numobjectPropJobs++;

    }


    // allocate stack for propagation job array
    int propArray[numobjectPropJobs];

    // add jobs to target propagation array
    idx = 0;
    for (i = 0; i < numObjs; i++)
    {
        if (exitCodes[i] == 0)
        {
            propArray[idx] = i;
            idx++;
        }

    }

    // copy memory
    memcpy(objectPropJobs, &propArray, sizeof(int)*numobjectPropJobs);
    // printf("done copying memory on rank %d \n",rank);


    if (rank == 0)
    {
        // update target states
        for (i = 0; i < numobjectPropJobs; i++)
        {
            // check for 
            //grab final state and index
            objIdx = objectPropJobs[i];
            finalState = objectTrajLengths[objIdx];
            finalIdx = finalState - 1;

            // convert to orbital elements
            R[0] = objectTrajectory[objIdx][finalIdx * 12 + 1];
            R[1] = objectTrajectory[objIdx][finalIdx * 12 + 2];
            R[2] = objectTrajectory[objIdx][finalIdx * 12 + 3];
            V[0] = objectTrajectory[objIdx][finalIdx * 12 + 4];
            V[1] = objectTrajectory[objIdx][finalIdx * 12 + 5];
            V[2] = objectTrajectory[objIdx][finalIdx * 12 + 6];

            // call fortran conversion routine
            cart2coe4c_(R, V, COE, &mu);

            //update states
            objectStates[objIdx][0] = objectTrajectory[objIdx][finalIdx * 12 + 0];
            objectStates[objIdx][1] = COE[0];
            objectStates[objIdx][2] = COE[1];
            objectStates[objIdx][3] = COE[2]*r2d;
            objectStates[objIdx][4] = COE[3]*r2d;
            objectStates[objIdx][5] = COE[4]*r2d;
            objectStates[objIdx][6] = COE[5]*r2d;
            objectStates[objIdx][7] = objectTrajectory[objIdx][finalIdx * 12 + 7];
            objectStates[objIdx][8] = objectTrajectory[objIdx][finalIdx * 12 + 8];
            objectStates[objIdx][9] = objectTrajectory[objIdx][finalIdx * 12 + 9];
            objectStates[objIdx][10] = objectTrajectory[objIdx][finalIdx * 12 + 10];
            objectStates[objIdx][11] = objectTrajectory[objIdx][finalIdx * 12 + 11];

        }

    }
    
    // Broadcast states from root rank
    for (i = 0; i < numobjectPropJobs; i++)
    {
        // send target state
        objIdx = objectPropJobs[i];
        MPI_Bcast(objectStates[objIdx], 12, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
    
    // update number of jobs
    (*numJobs) = numobjectPropJobs;

    // deallocate MPI derived type
    MPI_Type_free(&stateSend);

    return 0;

    

}