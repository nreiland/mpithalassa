#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include "semiContig.h"
#include "uniqueSemiContig.h"
#include "rowDouble.h"
#include "rowInt.h"
#include "contigDouble.h"
#include "contigInt.h"
#include "initializeCol.h"
#include "batchPropHead.h"
#include "propagate.h"
#include "output.h"
#include "update.h"

/*main function*/
int main( int argc, char *argv[] )
{

    // do MPI Stuff
    int  numtasks, rank, len, rc; 
    char hostname[MPI_MAX_PROCESSOR_NAME];
    int dest;
    double *rec, *sendObj;
    int root = 0;
    int tag = 1;
    int maxMxpts = 0;
    int send;
    int sender;
    int completedProcess;
    int remainder;
    int rootJobs, nomJobs;
    struct Input input;
    int i, j, k;
    int propStateIdx;
    double *partialTspan;
    int *mxpts;
    int *npts;
    int combs;
    struct jobParams jobs;
    struct objectsIn objects;
    double **objectsIn;
    double **objectStates;
    int deltaT;
    int *propJobs, *exitCodes;
    int *trajLengths;
    int numJobs, *objectMapping;
    struct uniqueDoubleSemi *objectTraj;
    int *globTargetMap;
    int L;

    /* global variables */
    double mu = 3.986004414498200E+05;

    // Memory Variables
    double memEstimate = 0;
    double sumAllocatedMem = 0;
    double sumDeltaMem = 0;
    double allocatedMem = 0;
    double prevMem = 0;
    double deltaMem = 0;
    double propJobsMem = 0;

    // get input propagation parameters
    input = getPropInputs(argv[1]);

    // read in target and field object states
    objects = readObjects(input.objects, input.numObjs);
    objectStates = objects.states;
    
    // initialize MPI
    MPI_Init(&argc,&argv);
    
    // get number of tasks 
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    
    // get my rank  
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    //  loop through chunks
    for (deltaT = 0; deltaT < input.numIntervals; deltaT++)
    {   
        // sum memory
        MPI_Allreduce(&allocatedMem, &sumAllocatedMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&deltaMem, &sumDeltaMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        // print memory use summary
        if (rank == 0) {
            printf("TIME CHUNK %d of %d\n",deltaT, input.numIntervals);
            printf("\n\n=============================================================================\n");
            printf("MEMORY INFORMATION\n");
            printf("Allocated memory (gigabytes) = %f\n",(sumAllocatedMem/1e+3));
            printf("Change in allocated memory (gigabytes) = %f\n", (sumDeltaMem/1e+3));
            printf("=============================================================================\n\n\n");
        }

        // on first iteration
        if (deltaT == 0)
        {

            // allocate memory for max propagations jobs
            propJobs = calloc(input.numObjs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            allocatedMem += (input.numObjs)*sizeof(int)/1e6;
            propJobsMem = input.numObjs*sizeof(int)/1e6;;

            // allocate memory for exit codes
            exitCodes = calloc(input.numObjs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            allocatedMem += (input.numObjs)*sizeof(int)/1e6;

            // populate propagation job arrays
            initPropArr(propJobs, input.numObjs);

            // intialize output directory
            initDirOut(input.outputDir, input.numIntervals, rank);

            // allocate memory for trajectory lengths
            trajLengths = calloc(input.numObjs, sizeof(int));
            allocatedMem += (input.numObjs)*sizeof(int)/1e6;

            // number of jobs
            numJobs = input.numObjs;
            MPI_Barrier(MPI_COMM_WORLD);

            // alloc mem for trajectory structs
            objectTraj = malloc(sizeof(struct uniqueDoubleSemi));
            allocatedMem += 2*sizeof(struct uniqueDoubleSemi)/1e6;    

            // allocate object mapping
            objectMapping = calloc(input.numObjs, sizeof(int));
            initMapping(objectMapping, input.numObjs);     
            allocatedMem += (input.numObjs)*sizeof(int)/1e6; 

            // allocate array of max points
            mxpts = calloc(input.numObjs, sizeof(int));
            allocatedMem += (input.numObjs)*sizeof(double)/1e6;

            // allocate array of number of output points
            npts = calloc(input.numObjs, sizeof(int));
            allocatedMem += (input.numObjs)*sizeof(double)/1e6;

            // allocate array of partial tspans
            partialTspan = calloc(input.numObjs, sizeof(double));
            allocatedMem += (input.numObjs)*sizeof(double)/1e6;

            // calculate partial tspans and max points
            calcTspans(input.numObjs, objects.timeSpans, partialTspan, mxpts, input.numIntervals, input.tstep, npts);

            // get max ammount of mxpts
            maxMxpts = 0;
            for(i = 0; i < input.numObjs; i++)
            {
                if (mxpts[i] > maxMxpts)
                    maxMxpts = mxpts[i];

            }

        }
        
        // calculate number of jobs per cpu for 1st round of filter
        jobs = calcJobLengths(numtasks, numJobs);

        // calculate max points
        // partialTspan = input.tspan/input.numIntervals;
        // mxpts = calcMxpts(partialTspan, input.tstep);
        // printf("max points = %d -- time step = %.15f -- tspan = %.15f\n",mxpts,input.tstep,partialTspan);

        // count number of trajectories
        if ( rank == 0)
            printf("allocating trajectories\n");
        if (rank == 0)
        {
            L = input.numObjs;
            // for (i = 0; i < L; i++)
            //     printf(" idx: %d -- %d\n",i,npts[i]);
        }
        else
        {
            L = 1;

        }

        // allocate/reallocate
        if (deltaT == 0)
        {
            if (rank == 0)
            {
            // printf("ranke %d allocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            alloc3DUniqueDoubleSemi(objectTraj, L, npts, 12);
            allocatedMem += ( *(objectTraj->mem) );
            }
            else
            {
                alloc3DUniqueDoubleSemi(objectTraj, L, &maxMxpts, 12);
                allocatedMem += ( *(objectTraj->mem) );
            }
            
        }
        else
        {
            if (rank > 0)
            {
            // printf("ranke %d reallocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            reallocUniqueDouble3Dsemi(objectTraj,L, &maxMxpts);
            allocatedMem += ( *(objectTraj->mem));
            allocatedMem -= ( *(objectTraj->prevmem));
            // printf("done on ranke %d\n",rank);
            }
        }

        // print memory allocation estimate main
        if(deltaT == 0)
        {
            MPI_Allreduce(&allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if(rank == 0)
                printf("Minmum memory requirement(gigabytes) = %f\n",(memEstimate/1e+3));
        }

        // propagateobjects
        if ( rank == 0 )
            printf("propagate 1st round of target objects -> %d\n",maxMxpts);
        propObjs(rank, jobs, input, objectTraj->arr, objectStates,
                    mxpts, partialTspan, input.tstep, mu, trajLengths, numJobs, propJobs, 
                    exitCodes, input.numObjs, &allocatedMem, 100, numtasks);

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        //UPDATE FOR NEXT ITERATION
        if ( rank == 0 )
            printf("updating for next iteration\n");
        updateStates(objectTraj->arr, objectStates, trajLengths, exitCodes, input.numObjs, propJobs, 
                     rank, &numJobs, &allocatedMem, &propJobsMem);
        
        //print output
        if ( rank == 0 )
            printf("printing chunk output\n");
        printOutput(objectTraj->arr, objectStates, input.outputDir, rank, trajLengths, input.numObjs, exitCodes, deltaT);
            
        // BREAK IF NO MORE JOBS REQUIRED
        if (numJobs == 0)
        {
            // printf("no more jobs required\n");
            break;
        }

        // update memory
        deltaMem = allocatedMem - prevMem;
        prevMem = allocatedMem;

    }
            
    if ( rank == 0 )
        printf("EXITING SUCCESSFULLY\n");

    // done with MPI  
    MPI_Finalize();

}
