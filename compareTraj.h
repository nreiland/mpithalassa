// MODULES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

/* MPI function to share and compare trajectories accross all processors */
int compareEndoTraj(int rank, int *trajLengths, double **targetTrajectories, struct Input input, int *globTargetMap,
                    struct endoJobParams jobParams, int mxpts, int **P, double *C, double dApproach, int combs, int *trajCompJobs, int *targetMapping)
{
    int i, j, k, njobs, idx, remIdx, jobType, jobIdx;
    int recTrajJobs[jobParams.rootNumTrajJobs + 1];
    int target, field, compPts, numApproaches, totApproaches, approachFlag, numFailures, failFlag;
    double delX, delY, delZ, rSep, mjd, flagDist;
    double sepArr[jobParams.rootNumTrajJobs + 1];
    double mjdArr[jobParams.rootNumTrajJobs + 1];
    int apprEventCount[jobParams.rootNumTrajJobs + 1];
    int jobPtr;
    int targetMapId, fieldMapId;
    int approachCount;
    int nomLength = jobParams.rootNumTrajJobs + 1;

    // define MPI Status
    MPI_Status stat;

    // derived MPI data types
    MPI_Datatype trajJobs;
    MPI_Datatype approachArr;
    MPI_Datatype colInfo;
    MPI_Type_contiguous(jobParams.rootNumTrajJobs + 1, MPI_INT, &trajJobs);
    MPI_Type_contiguous(jobParams.rootNumTrajJobs + 1, MPI_INT, &approachArr);
    MPI_Type_contiguous(jobParams.rootNumTrajJobs + 1, MPI_DOUBLE, &colInfo);
    MPI_Type_commit(&trajJobs);
    MPI_Type_commit(&approachArr);
    MPI_Type_commit(&colInfo);

    // MASTER PROCESSOR
    if (rank == 0)
    {
        // define array for collisions
        int locApproaches[jobParams.rootNumTrajJobs + 1];

        // define pointer arrays
        int trajJobPts[input.numCores][jobParams.rootNumTrajJobs + 1];

        // populate array of trajectory comparison job pointers

        // jobs for master processor
        idx = 0;
        trajJobPts[0][0] = jobParams.rootNumTrajJobs;
        for (i = 1; i <= jobParams.rootNumTrajJobs; i++)
        {
            trajJobPts[0][i] = idx;
            idx++;
        }

        // jobs for slave processors
        remIdx = 1;
        for (i = 1; i < input.numCores; i++)
        {
            // place job type in array
            if (remIdx < jobParams.trajRemainder)
                jobType = jobParams.rootNumTrajJobs;
            else
                jobType = jobParams.nomNumTrajJobs;
            
            trajJobPts[i][0] = jobType;
            
            // populate array with jobs
            for (j = 1; j <= jobType; j++)
            {
                trajJobPts[i][j] = idx;
                idx++;
            }

            remIdx++;
        }

        // DEBUG
        if (debug == 4)
        {
            printf("trajectory comparison job sends:\n");
            for (i = 0; i < input.numCores; i++)
            {
                printf("processor - %d\n", i);
                njobs = trajJobPts[i][0];
                for (j = 1; j <= njobs; j++)
                    printf("%d\n",trajJobPts[i][j]);
            }
        }

        // pass trajectory comparison jobs to other processors
        for(i = 1; i < input.numCores; i++)
        {
            MPI_Send(&trajJobPts[i][0], 1, trajJobs, i, i, MPI_COMM_WORLD);
        }

        // declare root jobs
        njobs = trajJobPts[0][0];
        recTrajJobs[0] = njobs;
        for (i = 1; i <= njobs; i++)
        {
            recTrajJobs[i] = trajJobPts[0][i];
        }

         // DEBUG
        if (debug == 4)
        {
            printf("\nI am processor %d... here are my jobs\n", rank);
            for (i = 1; i <= njobs; i++)
            {
            
                if (i == njobs)
                    printf("%d - %d\n", recTrajJobs[i], rank);
                else
                    printf("%d ", recTrajJobs[i]);
                
            }
        }

        // compare trajectories
        numApproaches = 0;
        for ( i = 1; i <= njobs; i++)
        {
            // get job index
            jobPtr = recTrajJobs[i];
            jobIdx = trajCompJobs[jobPtr];

            // get target and field object designations
            targetMapId = P[jobIdx][0];
            fieldMapId = P[jobIdx][1];
            target = globTargetMap[targetMapId];
            field = globTargetMap[fieldMapId];

            // check which trajectory has a shorter propagation time
            if (trajLengths[target] < trajLengths[field] )
                compPts = trajLengths[target];
            else
                compPts = trajLengths[field];
                        
            // step through trajectories
            approachFlag = 0;
            approachCount = 0;
            for (j = 0; j < compPts; j++)
            {
                // calculate relative separations distance
                delX = targetTrajectories[target][j * 12 + 1] - targetTrajectories[field][j * 12 + 1];
                delY = targetTrajectories[target][j * 12 + 2] - targetTrajectories[field][j * 12 + 2];
                delZ = targetTrajectories[target][j * 12 + 3] - targetTrajectories[field][j * 12 + 3];
                rSep =  sqrt(delX*delX + delY*delY + delZ*delZ);
                //printf("actively comparing trajectories - iter: %d\n",j);

                // check if a close approach has occurred
                if (rSep <= dApproach)
                {
                    if (approachCount == 0)
                    {
                        mjd = targetTrajectories[target][j * 12 + 0]; 
                        flagDist = rSep;
                    }
                    else if (approachCount > 0 && rSep < flagDist)
                    {
                        mjd = targetTrajectories[target][j * 12 + 0]; 
                        flagDist = rSep;
                    }
                    approachFlag = 1;
                    approachCount++;
                } 
            }

            // if an approach has occurred
            if (approachFlag == 1)
            {
                // update number of approaches
                numApproaches++;
            
                // add to local approach arrays
                locApproaches[numApproaches] = jobIdx;
                mjdArr[numApproaches] = mjd;
                sepArr[numApproaches] = flagDist;
                apprEventCount[numApproaches] = approachCount;
                
            }
        
        }

        // add total number of approaches to locApproaches array
        locApproaches[0] = numApproaches;

        // synchronize all cores
        //MPI_Barrier(MPI_COMM_WORLD);

        // calculate total number of approaches
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // combine
        idx = 0;

        // add root close approaches
        for (j = 1; j <= numApproaches; j++)
        {
            jobIdx = locApproaches[j];
            target = P[jobIdx][0];
            field = P[jobIdx][1];
            C[idx * 5 + 0] = target;
            C[idx * 5 + 1] = field;
            C[idx * 5 + 2] = sepArr[j];
            C[idx * 5 + 3] = mjdArr[j];
            C[idx * 5 + 4] = apprEventCount[j];
            idx++;
        }

        // gather from other cores
        for (i = 1; i < input.numCores; i++)
        {
            MPI_Recv(&numApproaches, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &stat);
            MPI_Recv(locApproaches, jobParams.rootNumTrajJobs + 1, approachArr, i, 1, MPI_COMM_WORLD, &stat);
            MPI_Recv(sepArr, jobParams.rootNumTrajJobs + 1, colInfo, i, 2, MPI_COMM_WORLD, &stat);
            MPI_Recv(mjdArr, jobParams.rootNumTrajJobs + 1, colInfo, i, 3, MPI_COMM_WORLD, &stat);
            MPI_Recv(apprEventCount, jobParams.rootNumTrajJobs + 1, approachArr, i, 4, MPI_COMM_WORLD, &stat);

            for (j = 1; j <= numApproaches; j++)
            {
                jobIdx = locApproaches[j];
                target = P[jobIdx][0];
                field = P[jobIdx][1];
                C[idx * 5 + 0] = target;
                C[idx * 5 + 1] = field;
                C[idx * 5 + 2] = sepArr[j];
                C[idx * 5 + 3] = mjdArr[j];
                C[idx * 5 + 4] = apprEventCount[j];
                idx++;
            }
        }

    }

    // SLAVE PROCESS
    else
    {
        // define array for collisions
        int locApproaches[jobParams.rootNumTrajJobs + 1];

        // receive trajectory comparison jobs from master processors
        MPI_Recv(recTrajJobs, 1 + jobParams.rootNumTrajJobs, trajJobs, 0, rank, MPI_COMM_WORLD, &stat);
        njobs = recTrajJobs[0];


        // DEBUG
        if (debug == 4)
        {
            printf("\nI am processor %d... here are my jobs\n", rank);
            for (i = 1; i <= njobs; i++)
            {
            
                if (i == njobs)
                    printf("%d - %d\n", recTrajJobs[i], rank);
                else
                    printf("%d ", recTrajJobs[i]);
                
            }
        }

        // compare trajectories
        numApproaches = 0;
        for ( i = 1; i <= njobs; i++)
        {
            // get job index
            jobPtr = recTrajJobs[i];
            jobIdx = trajCompJobs[jobPtr];

            // get target and field object designations
            targetMapId = P[jobIdx][0];
            fieldMapId = P[jobIdx][1];
            target = globTargetMap[targetMapId];
            field = globTargetMap[fieldMapId];

            // check which trajectory has a shorter propagation time
            if (trajLengths[target] < trajLengths[field] )
                compPts = trajLengths[target];
            else
                compPts = trajLengths[field];
            
            // step through trajectories
            approachFlag = 0;
            approachCount = 0;
            for (j = 0; j < compPts; j++)
            {
                //printf("inter %d on %d\n",j,rank);
                // calculate relative separations distance
                delX = targetTrajectories[target][j * 12 + 1] - targetTrajectories[field][j * 12 + 1];
                delY = targetTrajectories[target][j * 12 + 2] - targetTrajectories[field][j * 12 + 2];
                delZ = targetTrajectories[target][j * 12 + 3] - targetTrajectories[field][j * 12 + 3];
                rSep =  sqrt(delX*delX + delY*delY + delZ*delZ);

                // check if a close approach has occurred
                if (rSep <= dApproach)
                {
                    if (approachCount == 0)
                    {
                        mjd = targetTrajectories[target][j * 12 + 0]; 
                        flagDist = rSep;
                    }
                    else if (approachCount > 0 && rSep < flagDist)
                    {
                        mjd = targetTrajectories[target][j * 12 + 0]; 
                        flagDist = rSep;
                    }
                    approachFlag = 1;
                    approachCount++;
                } 
            }

            // if an approach has occurred
            if (approachFlag == 1)
            {
                // update number of approaches
                numApproaches++;
            
                // add to local approach array
                locApproaches[numApproaches] = jobIdx;
                mjdArr[numApproaches] = mjd;
                sepArr[numApproaches] = flagDist;
                apprEventCount[numApproaches] = approachCount;
                
            }
        
        }

        // add total number of approaches to locApproaches array
        locApproaches[0] = numApproaches;

        // synchronize all cores
        //MPI_Barrier(MPI_COMM_WORLD);

        // calculate total number of approaches
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // send local approach array to master processor
        MPI_Send(&numApproaches, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&locApproaches[0], 1, approachArr, 0, 1, MPI_COMM_WORLD);
        MPI_Send(&sepArr[0], 1, colInfo, 0, 2, MPI_COMM_WORLD);
        MPI_Send(&mjdArr[0], 1, colInfo, 0, 3, MPI_COMM_WORLD);
        MPI_Send(&apprEventCount[0], 1, approachArr, 0, 4, MPI_COMM_WORLD);

    }

    // synchronize processors
    //MPI_Barrier (MPI_COMM_WORLD);

    // send all other processors C array
    MPI_Bcast(C, 5*combs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Debug
    if ( debug == 3)
    {
        printf("core of rank: %d\n", rank);
        for (i = 0; i < totApproaches; i++)
        {
            printf("%f %f %f %f - %d\n", C[i * 4 + 0], C[i * 4 + 1], C[i * 4 + 2],
                C[i * 4 + 3], rank);
        }
    }

    // deallocate derived data types
    MPI_Type_free(&trajJobs);
    MPI_Type_free(&approachArr);
    MPI_Type_free(&colInfo);

    return totApproaches;

}

/* MPI function to share and compare exogenous trajectories accross all processors */
int compareExoTraj(int rank, int *targTrajLengths, int *fieldTrajLengths, double **targetTrajectories, 
                    double **fieldTrajectories, struct Input input,struct exoJobParams jobParams, int mxpts, 
                    int **P, struct contigDouble *C, int *trajCompJobs, int *targetMap, int *fieldMap, double dApproach, 
                    int combs, int cpus, int *pairs, struct contigInt *locApproaches, struct contigDouble *sepArr, 
                    struct contigDouble *mjdArr, double *cpuMem, struct redStates *nextRed, int *targExitCodes, int *fieldExitCodes)
{
    int i, j, k, njobs, idx, remIdx, jobType, jobIdx;
    //int recTrajJobs[jobParams.rootNumTrajJobs + 1];
    int target, field, compPts, numApproaches, totApproaches, approachFlag, failFlag;
    double delX, delY, delZ, rSep, mjd, flagDist;
    int jobPtr;
    int targetMapId, fieldMapId;
    int approachCount;
    int nomLength = jobParams.rootNumTrajJobs + 1;
    // int *compJobs;
    int compJobs[jobParams.rootNumTrajJobs + 1];
    int targId, fieldId, Pidx, cpuIdx, targLength, fieldLength, tag, tag1, tag2;
    int locNumJobs;
    int length;
    int combos = 0;

    // define MPI Status
    MPI_Status stat;

    // derived MPI data types
    MPI_Datatype trajJobs;
    MPI_Datatype targSend;
    MPI_Datatype fieldSend;
    MPI_Type_contiguous(jobParams.rootNumTrajJobs + 1, MPI_INT, &trajJobs);
    MPI_Type_contiguous(12*mxpts, MPI_DOUBLE, &targSend);
    MPI_Type_contiguous(12*mxpts, MPI_DOUBLE, &fieldSend);
    MPI_Type_commit(&trajJobs);
    MPI_Type_commit(&targSend);
    MPI_Type_commit(&fieldSend);

    // zero Pcount
    // memset(&Pcount,0,combs*sizeof(int));

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < combs; i++)
    {
        // assign
        if(cpuIdx == rank)
        {
            compJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if exceeds num cpus
        if(cpuIdx == cpus)
            cpuIdx = 1;
        
    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;

    // MASTER PROCESSOR
    if (rank == 0)
    {
        int Pcount[combs];
        int Pbuff[combs][2];

        // zero Pcount
        memset(&Pcount,0,combs*sizeof(int));

        // define array for collisions
        //int locApproaches[jobParams.rootNumTrajJobs + 1]
        

        // send trajectories
        cpuIdx = 1;
        for(i = 0; i < combs; i++)
        {
            // set Ids
            // printf("STARTING TRAJECTORY SENDING\n");
            Pidx = trajCompJobs[i];
            targetMapId = P[Pidx][0];
            fieldMapId = P[Pidx][1];
            targId = targetMap[targetMapId];
            fieldId = fieldMap[fieldMapId];
            // printf(" Sending from ROOT --     index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",i,combs,Pidx,targetMapId,fieldMapId,targId,fieldId);

            // send trajectories
            targLength = targTrajLengths[targId];
            fieldLength = fieldTrajLengths[fieldId];
            tag1 = Pidx;
            tag2 = Pidx + 1000;
            MPI_Send(targetTrajectories[targId], 1, targSend, cpuIdx, tag1, MPI_COMM_WORLD);
            MPI_Send(fieldTrajectories[fieldId], 1, fieldSend, cpuIdx, tag2, MPI_COMM_WORLD);

            // update cpuIdx
            cpuIdx++;

            // reset cpuIdx if equal to number of cpus
            if(cpuIdx == cpus)
                cpuIdx = 1;

        }
        // printf("DONE WITH SENDS\n");

        // calculate total number of approaches
        numApproaches = 0;
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // reallocate C array if necessary
        if(4*totApproaches > *(C->elements))
        {   
            reallocContigDouble(C, 2*4*totApproaches);
            *(cpuMem) += *(C->mem);
            *(cpuMem) -= *(C->prevmem);
            
        }

        // gather from other cores
        idx = 0;
        for (i = 1; i < cpus; i++)
        {
            MPI_Recv(&numApproaches, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &stat);

            // reallocate arrays if necessary
            if(numApproaches > *(locApproaches->elements))
            {
                reallocContigInt(locApproaches, 2*numApproaches);
                reallocContigDouble(mjdArr, 2*numApproaches);
                reallocContigDouble(sepArr, 2*numApproaches);
                *(cpuMem) += ( *(locApproaches->mem) + *(mjdArr->mem) + *(sepArr->mem) );
                *(cpuMem) -= ( *(locApproaches->prevmem) + *(mjdArr->prevmem) + *(sepArr->prevmem) ); 

            }

            // get arrays
            if (numApproaches > 0)
            {
                MPI_Recv(locApproaches->arr, numApproaches, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
                MPI_Recv(sepArr->arr, numApproaches, MPI_DOUBLE, i, 2, MPI_COMM_WORLD, &stat);
                MPI_Recv(mjdArr->arr, numApproaches, MPI_DOUBLE, i, 3, MPI_COMM_WORLD, &stat);
            }
            for (j = 0; j < numApproaches; j++)
            {   
                Pidx = locApproaches->arr[j];
                target = P[Pidx][0];
                field = P[Pidx][1];
                C->arr[idx * 4 + 0] = (double)target;
                C->arr[idx * 4 + 1] = (double)field;
                C->arr[idx * 4 + 2] = sepArr->arr[j];
                C->arr[idx * 4 + 3] = mjdArr->arr[j];
                idx++;

                // count combinations
                if(Pcount[Pidx] == 0)
                {
                    
                    Pbuff[combos][0] = target;
                    Pbuff[combos][1] = field;
                    Pcount[Pidx] = 1;
                    combos++;

                    // reallocate if necessary

                    
                }
            }
        }

        // reallocate next P if necessary
        printf("begining to reallocate P\n");
        reallocRowInt(nextRed->P, combos);
        printf("finished reallocating\n");
        *(cpuMem) += *(nextRed->P->mem);
        *(cpuMem) -= *(nextRed->P->prevmem);
        printf("finished counting mem\n");
        // save Pbuffer to next P array
        for(i = 0; i < combos; i++)
        {
            nextRed->P->arr[i][0] = Pbuff[i][0];
            nextRed->P->arr[i][1] = Pbuff[i][1];
        }

    }

    // SLAVE PROCESS
    else
    {

        // init number of approaches
        numApproaches = 0;

        for(i = 0; i < locNumJobs; i++)
        {
            jobIdx = compJobs[i];
            Pidx = trajCompJobs[jobIdx];
            targetMapId = P[Pidx][0];
            fieldMapId = P[Pidx][1];
            targId = targetMap[targetMapId];
            fieldId = fieldMap[fieldMapId]; 
            // printf(" Receiving from RANK %d -- index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",rank,i,locNumJobs,Pidx,targetMapId,fieldMapId,targId,fieldId); 

            // get trajectories
            tag1 = Pidx;
            tag2 = Pidx + 1000;

            // printf("looking for job %d on cpu %d - iter: %d --- %d points\n",tag1,rank,i,mxpts);
            MPI_Recv(targetTrajectories[0], 12*mxpts, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD, &stat);
            MPI_Recv(fieldTrajectories[0], 12*mxpts, MPI_DOUBLE, 0, tag2, MPI_COMM_WORLD, &stat);
            // printf("received job %d on cpu %d\n",tag1,rank);

            // skip if propagation has failed
            if(targExitCodes[targId] == 1 || fieldExitCodes[fieldId] == 1 )
            {
                printf("propagation failure detected on comparison of target %d (%d) and field %d (%d)...skipping\n",targId,targExitCodes[targId],fieldId,fieldExitCodes[fieldId]);
                continue;
            }

            // get lengths
            targLength = targTrajLengths[targId];
            fieldLength = fieldTrajLengths[fieldId];
            

            // check which trajectory has a shorter propagation time
            if (targTrajLengths[targId] < fieldTrajLengths[fieldId])
                compPts = targTrajLengths[targId];
            else
                compPts = fieldTrajLengths[fieldId];
            // printf("rank_%d comparing target %d with field %d over %d points\n",rank,targId,fieldId,compPts);
            // step through trajectories

            for (j = 0; j < compPts; j++)
            {
                // calculate relative separations distance
                delX = targetTrajectories[0][j * 12 + 1] - fieldTrajectories[0][j * 12 + 1];
                delY = targetTrajectories[0][j * 12 + 2] - fieldTrajectories[0][j * 12 + 2];
                delZ = targetTrajectories[0][j * 12 + 3] - fieldTrajectories[0][j * 12 + 3];
                rSep =  sqrt(delX*delX + delY*delY + delZ*delZ);

                // check if a close approach has occurred
                if (rSep <= dApproach)
                {
                    // save approach conditions
                    mjd = targetTrajectories[0][j * 12 + 0]; 
                    flagDist = rSep;

                    // add to local approach array
                    locApproaches->arr[numApproaches] = Pidx;
                    mjdArr->arr[numApproaches] = mjd;
                    sepArr->arr[numApproaches] = flagDist;

                    // update number of approaches
                    numApproaches++;

                    // reallocate arrays if necessary
                    if(numApproaches == *(locApproaches->elements))
                    {    
                        reallocContigInt(locApproaches, 2*numApproaches);
                        reallocContigDouble(mjdArr, 2*numApproaches);
                        reallocContigDouble(sepArr, 2*numApproaches);
                        *(cpuMem) += ( *(locApproaches->mem) + *(mjdArr->mem) + *(sepArr->mem) );
                        *(cpuMem) -= ( *(locApproaches->prevmem) + *(mjdArr->prevmem) + *(sepArr->prevmem) );
                    }  
                    
                } 

            }

            

        }
        // printf("finished on rank %d\n",rank);

        // calculate total number of approaches
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // send local approach array to master processor
        // printf("sending %d from %d\n",numApproaches,rank);
        MPI_Send(&numApproaches, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        if (numApproaches > 0)
        {
            MPI_Send(locApproaches->arr, numApproaches, MPI_INT, 0, 1, MPI_COMM_WORLD);
            MPI_Send(sepArr->arr, numApproaches, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD);
            MPI_Send(mjdArr->arr, numApproaches, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD);
        }

    }
    
    // broadcast number of combinations
    MPI_Bcast(&combos, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // reallocate next P if necessary
    reallocRowInt(nextRed->P, combos);
    *(cpuMem) += *(nextRed->P->mem);
    *(cpuMem) -= *(nextRed->P->prevmem);

    // send all other processors P array
    for (i = 0; i < combos; i++)
        MPI_Bcast(nextRed->P->arr[i], 2, MPI_INT, 0, MPI_COMM_WORLD);

    // dellocate MPI derived types
    MPI_Type_free(&trajJobs);
    MPI_Type_free(&targSend);
    MPI_Type_free(&fieldSend);

    // save number of combos
    *(pairs) = combos;

    return totApproaches;

}