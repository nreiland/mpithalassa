// header file for fica memory management
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

struct doubleSemi
{
    int *l;
    int *rows;
    int *columns;
    double **arr;
    double *mem;
    double *prevmem;
};

struct rowDouble
{
    double **arr;
    int *rows;
    int *columns;
    double *mem;
    double *prevmem;
};

struct rowInt
{
    int **arr;
    int *rows;
    int *columns;
    double *mem;
    double *prevmem;
};

struct contigDouble 
{
    int *elements;
    double *arr;
    double *mem;
    double *prevmem;
};

struct contigInt 
{
    int *elements;
    int *arr;
    double *mem;
    double *prevmem;
};

void alloc3DDoubleSemi(struct doubleSemi *array, int newl, int newrows, int newcolumns)
{
    int i;

    // define values in structure

    // allocate mem for organizational pointers
    array->l = malloc(sizeof(int));
    // array->prevl = malloc(sizeof(int *));
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    // array->prevrows = malloc(sizeof(int *));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double))
    
    // define pointer values for organization pointers
    *array->l = newl;
    // *(array->prevl) = prevl;
    *(array->rows) = newrows;
    *(array->columns) = newcolumns;
    // *(array->prevrows) = rows;

    // allocate memory for main array
    array->arr = malloc(newl * sizeof(double *));
    for (i = 0; i < newl; i++) {
        array->arr[i] = (double *)malloc(newrows*newcolumns*sizeof(double));
        memset(array->arr[i], 0, newrows*newcolumns*sizeof(double));
    }

    // count memory
    *(array->mem) = 3*sizeof(int *);
    *(array->mem) += newl*sizeof(double *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += sizeof(double **);
    *(array->mem) += newl*newrows*newcolumns*sizeof(double);
    *(array->prevmem) = 0;

}

int allocRowDouble(struct rowDouble *array, int rows, int columns) {

    int i;

    // define values in structure

    // allocate mem for organizational pointers
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    // array->prevrows = malloc(sizeof(int *));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double))
    
    // define pointer values for organization pointers
    *(array->rows) = rows;
    *(array->columns) = columns;
    // *(array->prevrows) = rows;

    // allocate memory for main array
    array->arr = malloc(rows * sizeof(double *));
    for (i = 0; i < l; i++) {
        arr[i] = (double *)malloc(columns*sizeof(double));
        memset(arr[i], 0, columns*sizeof(double))
    }

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += rows*sizeof(double *);
    *(array->mem) += sizeof(double **);
    *(array->) += rows*columns*sizeof(double);
    *(array->prevmem) = 0;


    // return value
    return 0;

}

int allocRowInt(struct rowInt *array, int rows, int columns) {

    int i;

    // define values in structure

    // allocate mem for organizational pointers
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    // array->prevrows = malloc(sizeof(int *));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double))
    
    // define pointer values for organization pointers
    *(array->rows) = rows;
    *(array->columns) = columns;
    // *(array->prevrows) = rows;

    // allocate memory for main array
    array->arr = malloc(rows * sizeof(int *));
    for (i = 0; i < l; i++) {
        arr[i] = (int *)malloc(columns*sizeof(int));
        memset(arr[i], 0, columns*sizeof(int))
    }

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += rows*sizeof(int *);
    *(array->mem) += sizeof(int **);
    *(array->mem) += rows*columns*sizeof(int);
    *(array->mem) += 2*sizeof(double *);
    *(array->prevmem) = 0;


    // return value
    return 0;

}

int allocContigDouble(struct contigDouble *array, int elements) {

    int i;

    // define values in structure

    // allocate mem for organizational pointers
    array->elements = malloc(sizeof(int));
    // array->prevelements = malloc(sizeof(int *));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double))
    
    // define pointer values for organization pointers
    *(array->elements) = elements;
    // *(array->prevelements) = elements;

    // allocate memory for main array
    array->arr = malloc(elements * sizeof(double));
    memset(arr, 0, elements*sizeof(double))

    // count memory
    *(array->mem) = sizeof(int *);
    *(array->mem) += elements*sizeof(double);
    *(array->mem) += 2*sizeof(double *);
    *(array->prevmem) = 0;

    // return value
    return 0;

}

int allocContigInt(struct contigInt *array, int elements) {

    int i;

    // define values in structure

    // allocate mem for organizational pointers
    array->elements = malloc(sizeof(int));
    // array->prevelements = malloc(sizeof(int *));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double))
    
    // define pointer values for organization pointers
    *(array->elements) = elements;
    // *(array->prevelements) = elements;

    // allocate memory for main array
    array->arr = malloc(elements * sizeof(int));
    memset(arr, 0, elements*sizeof(int))

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += elements*sizeof(int);
    *(array->mem) += 2*sizeof(double *);
    *(array->prevmem) = 0;

    // return value
    return 0;

}

int reallocDouble3Dsemi(struct double3Dsemi *array, int newl, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevl = *(array->l);
    int prevrows = *(array->rows);
    
    // define factor of safety
    double fs = 1.5;
    if ( newl > prevl )
        newl = fs*newl;
    else if ( newrows > prevrows)
        newrows = fs*newrows;

    // flag = 1: newl & newrows, flag = 2: newl * prevrows, flag = 3: prevl & newrows, flag = 4: prevl & prevrows
    int flag = 0; 
    
    // reallocate double pointer if l has increased -> use newl
    if ( newl > prevl ) {

        // printf(" reallocating memory address: %p\n", arrary->arr );
        // printf("prevl = %d - - -  l = %d\n", prevl , newl );

        // reallocate double pointer
        arrary->arr = realloc( arr->array, newl*sizeof(double *) );

        // reallocate pointers if number of rows has increased -> use newrows
        if ( newrows > prevrows ) {

            // reallocate old pointers
            for (i = 0; i < prevl; i++) {
                array->arr[i] = realloc( array->arr[i], newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }
            // allocate new pointers
            for (i = prevl; i < newl; i++) {
                array->arr[i] = malloc(newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }

            flag = 1;
        }
        // set pointed to memory to zero if number of rows has not increased -> use prevrows
        else {

            // reallocate old pointers
            for (i = 0; i < prevl; i++) {
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            }
            // allocate new pointers
            for (i = prevl; i < newl; i++) {
                array->arr[i] = malloc(prevrows*columns*sizeof(double));
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            }

            flag = 2;
        }
    }
    // if l has not increased check if number of rows has increased -> use prevl
    else {

        // reallocate pointers if number of rows has increased -> use newrows
        if ( newrows > prevrows ) {
            for (i = 0; i < prevl; i++) {
                array->arr[i] = realloc( array->arr[i], newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }

            flag = 3;
        }
        // set pointed to memory to zero if number of rows has not increased -> use prevrows
        else {
            for (i = 0; i < prevl; i++)
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            
            flag = 4;
        }

    }

    // calculate new memory based on flag and save new values to struct

    // newl & newrows
    if ( flag == 1 ) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newl*sizeof(double *);
        newmem += sizeof(double **);
        newmem += newl*newrows*columns*sizeof(double);

        *(array->l) = newl;
        *(array->rows) = newrows;
    }
    // newl & prevrows
    else if (flag == 2) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newl*sizeof(double *);
        newmem += sizeof(double **);
        newmem += newl*prevrows*columns*sizeof(double);

        *(array->l) = newl;
        *(array->rows) = prevrows;
    }
    // prevl & newrows
    else if (flag == 3) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevl*sizeof(double *);
        newmem += sizeof(double **);
        newmem += prevl*newrows*columns*sizeof(double);

        *(array->l) = prevl;
        *(array->rows) = newrows;
    }
    // prevl & newrows
    else if (flag == 4) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevl*sizeof(double *);
        newmem += sizeof(double **);
        newmem += prevl*prevrows*columns*sizeof(double);

        *(array->l) = prevl;
        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int -> later add capability to check for alloc failures
    return 0;

}

int reallocRowDouble(struct rowDouble *array, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevrows = *(array->rows);

    // define factor of safety
    double fs = 1.5;
    if ( newrows > prevrows )
        newrows = fs*newrows;

    // flag = 1: new rows, flag = 2: prevrows
    int flag = 0; 
    
    // reallocate double pointer if number of rows has increased -> use newrows
    if ( newrows > prevrows ) {

        // printf(" reallocating memory address: %p\n", arrary->arr );
        // printf("prevrows = %d - - -  newrows = %d\n", prevrows , newrows );

        // reallocate double pointer
        arrary->arr = realloc( arr->array, newrows*sizeof(double *) );

        // reallocate old rows
        for (i = 0; i < prevrows; i++) {
            array->arr[i] = realloc( array->arr[i], columns*sizeof(double) );
            memset( array->arr[i], 0, columns*sizeof(double) );
        }
        // allocate new rows
            for (i = prevrows; i < newrows; i++) {
            array->arr[i] = malloc( columns*sizeof(double) );
            memset( array->arr[i], 0, columns*sizeof(double) );
        }

        flag = 1;
    }
    // if rows have not increased check if number of rows has increased -> use prevl
    else {

        // set values to zeros
        for (i = 0; i < newrows; i++) {
            memset( array->arr[i], 0, columns*sizeof(double) );
        }

        flag = 2;

    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newrows*sizeof(double *);
        newmem += sizeof(double **);
        newmem += newrows*columns*sizeof(double);

        *(array->rows) = newrows;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevrows*sizeof(double *);
        newmem += sizeof(double **);
        newmem += prevrows*columns*sizeof(double);

        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int
    return 0;

}

int reallocRowInt(struct rowInt *array, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevrows = *(array->rows);

    // define factor of safety
    double fs = 1.5;
    if ( newrows > prevrows )
        newrows = fs*newrows;

    // flag = 1: new rows, flag = 2: prevrows
    int flag = 0; 
    
    // reallocate double pointer if number of rows has increased -> use newrows
    if ( newrows > prevrows ) {

        // printf(" reallocating memory address: %p\n", arrary->arr );
        // printf("prevrows = %d - - -  newrows = %d\n", prevrows , newrows );

        // reallocate double pointer
        arrary->arr = realloc( arr->array, newrows*sizeof(int *) );

        // reallocate old rows
        for (i = 0; i < prevrows; i++) {
            array->arr[i] = realloc( array->arr[i], columns*sizeof(int) );
            memset( array->arr[i], 0, columns*sizeof(int) );
        }
        // allocate new rows
            for (i = prevrows; i < newrows; i++) {
            array->arr[i] = malloc( columns*sizeof(int) );
            memset( array->arr[i], 0, columns*sizeof(int) );
        }

        flag = 1;
    }
    // if rows have not increased check if number of rows has increased -> use prevl
    else {

        // set values to zeros
        for (i = 0; i < newrows; i++) {
            memset( array->arr[i], 0, columns*sizeof(int) );
        }

        flag = 2;

    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newrows*sizeof(int *);
        newmem += sizeof(int **);
        newmem += newrows*columns*sizeof(int);

        *(array->rows) = newrows;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevrows*sizeof(int *);
        newmem += sizeof(int **);
        newmem += prevrows*columns*sizeof(int);

        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int
    return 0;

}

int reallocContigDouble(struct contigDouble *array, int elements) {

    // define vars
    int i;
    int prevElements = *(array->elements);
    int newElements = *(array->elements);
    double newMem = 0;
    double prevMem = *(array->mem);

    // define factor of safety
    double fs = 1.5;
    if ( newElements > prevElements )
        newElements = fs*newElements;

    // realloc if number of elements has increased
    if (newElements > prevElements) {
        array->arr = realloc(array->arr, newElements*sizeof(double));
        memset(array->arr, 0, newElements*sizeof(double));

        flag = 1;
    }
    // set memory to zero if no reallocation
    else {
        memset(array->arr, 0, prevElements*sizeof(double));

        flag = 2;
    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = sizeof(int *);
        newmem += newElements*sizeof(double);
        newmem += 3*sizeof(double *);

        *(array->elements) = newElements;
    }
    // prevrows
    else if (flag == 2) {
        newmem = sizeof(int *);
        newmem += prevElements*sizeof(double);
        newmem += 3*sizeof(double *);

        *(array->elements) = prevElements;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int
    return 0;
}

int reallocContigInt(struct contigDouble *array, int elements) {

    // define vars
    int i;
    int prevElements = *(array->elements);
    int newElements = *(array->elements);
    double newMem = 0;
    double prevMem = *(array->mem);

    // define factor of safety
    double fs = 1.5;
    if ( newElements > prevElements )
        newElements = fs*newElements;

    // realloc if number of elements has increased
    if (newElements > prevElements) {
        array->arr = realloc(array->arr, newElements*sizeof(int));
        memset(array->arr, 0, newElements*sizeof(int));

        flag = 1;
    }
    // set memory to zero if no reallocation
    else {
        memset(array->arr, 0, prevElements*sizeof(int));

        flag = 2;
    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += newElements*sizeof(int);
        newmem += 2*sizeof(double *);

        *(array->elements) = newElements;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += prevElements*sizeof(int);
        newmem += 2*sizeof(double *);

        *(array->elements) = prevElements;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int
    return 0;
}


