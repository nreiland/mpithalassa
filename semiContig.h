// header file for fica memory management

struct doubleSemi
{
    int *l;
    int *rows;
    int *columns;
    double **arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 3d semi contiguous array structure*/
 void alloc3DDoubleSemi(struct doubleSemi *array, int newl, int newrows, int newcolumns) {

    int i;
    double buff = 0;

    // allocate mem for organizational pointers
    array->l = malloc(sizeof(int));
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double));

    // define pointer values for organization pointers
    *(array->l) = newl;
    *(array->rows) = newrows;
    *(array->columns) = newcolumns;
    // allocate memory for main array
    array->arr = (double **)malloc(newl * sizeof(double *));
    for (i = 0; i < newl; i++) {
        array->arr[i] = (double *)malloc(newrows*newcolumns*sizeof(double));
        memset(array->arr[i], 0, newrows*newcolumns*sizeof(double));
    }


    // count memory
    *(array->mem) = 3*sizeof(int *);
    *(array->mem) += newl*sizeof(double *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += sizeof(double **);
    buff = newl*newcolumns*sizeof(double)/1e6;
    buff = buff*newrows;
    *(array->mem) = *(array->mem)/1e6 + buff;
    // printf("test bytes1 == %lu || test bytes2 == %f\n",newl*newrows*newcolumns*sizeof(double),newl*newrows*newcolumns*sizeof(double));
    *(array->prevmem) = 0;

}

/* function to reallocate 3d semi contiguous array structure*/
int reallocDouble3Dsemi(struct doubleSemi *array, int newl, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevl = *(array->l);
    int prevrows = *(array->rows);
    double buff;
    
    // define factor of safety
    double fs = 1.0;
    if ( newl > prevl )
        newl = fs*newl;
    else if ( newrows > prevrows)
        newrows = fs*newrows;

    // flag = 1: newl & newrows, flag = 2: newl * prevrows, flag = 3: prevl & newrows, flag = 4: prevl & prevrows
    int flag = 0; 
    
    // reallocate double pointer if l has increased -> use newl
    if ( newl > prevl ) {
        // printf(" reallocating memory address: %p\n", array->arr );
        // printf("prevl = %d - - -  l = %d\n", prevl , newl );

        // reallocate double pointer
        array->arr = realloc( array->arr, newl*sizeof(double *) );

        // reallocate pointers if number of rows has increased -> use newrows
        if ( newrows > prevrows ) {

            // reallocate old pointers
            for (i = 0; i < prevl; i++) {
                array->arr[i] = realloc( array->arr[i], newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }
            // allocate new pointers
            for (i = prevl; i < newl; i++) {
                array->arr[i] = (double *)malloc(newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }

            flag = 1;
        }
        // set pointed to memory to zero if number of rows has not increased -> use prevrows
        else {

            // reallocate old pointers
            for (i = 0; i < prevl; i++) {
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            }
            // allocate new pointers
            for (i = prevl; i < newl; i++) {
                array->arr[i] = (double *)malloc(prevrows*columns*sizeof(double));
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            }

            flag = 2;
        }
    }
    // if l has not increased check if number of rows has increased -> use prevl
    else {

        // reallocate pointers if number of rows has increased -> use newrows
        if ( newrows > prevrows ) {
            for (i = 0; i < prevl; i++) {
                array->arr[i] = realloc( array->arr[i], newrows*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows*columns*sizeof(double) );
            }

            flag = 3;
        }
        // set pointed to memory to zero if number of rows has not increased -> use prevrows
        else {
            for (i = 0; i < prevl; i++)
                memset( array->arr[i], 0, prevrows*columns*sizeof(double) );
            
            flag = 4;
        }

    }

    // calculate new memory based on flag and save new values to struct

    // newl & newrows
    if ( flag == 1 ) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newl*sizeof(double *);
        newmem += sizeof(double **);
        buff = newl*columns*sizeof(double)/1e6;
        buff = buff*newrows;
        newmem = newmem/1e6 + buff;

        *(array->l) = newl;
        *(array->rows) = newrows;
    }
    // newl & prevrows
    else if (flag == 2) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newl*sizeof(double *);
        newmem += sizeof(double **);
        buff = newl*columns*sizeof(double)/1e6;
        buff = buff*prevrows;
        newmem = newmem/1e6 + buff;

        *(array->l) = newl;
        *(array->rows) = prevrows;
    }
    // prevl & newrows
    else if (flag == 3) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevl*sizeof(double *);
        newmem += sizeof(double **);
        buff = prevl*columns*sizeof(double)/1e6;
        buff = buff*newrows;
        newmem = newmem/1e6 + buff;

        *(array->l) = prevl;
        *(array->rows) = newrows;
    }
    // prevl & newrows
    else if (flag == 4) {
        newmem = 3*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevl*sizeof(double *);
        newmem += sizeof(double **);
        buff = prevl*columns*sizeof(double)/1e6;
        buff = buff*prevrows;
        newmem = newmem/1e6 + buff;

        *(array->l) = prevl;
        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem;

    // return int -> later add capability to check for alloc failures
    return 0;

}
   