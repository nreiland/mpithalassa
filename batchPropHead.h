// header file for thalassa batch propagation
// created by Nathan Reiland
// 3/15/2019

/* DEFINE CONSTANTS */
#define MAXLINE 1000
#define debug 6

/* STRUCTURE DEFINITIONS */
struct Input
{
    char outputDir[MAXLINE];
    char objects[MAXLINE];
    int numObjs;
    int numIntervals;
    int insgrav;
    int isun;
    int imoon;
    int idrag;
    int iF107;
    int iSRP;
    int iephem;
    int gdeg;
    int gord;
    double tol;
    double tspan; 
    double tstep;
    int imcoll;
    int eqs;
};


struct allocIntOut
{
    int **point;
    double mem;

};

struct allocDoubOut
{
    double **point;
    double mem;
};

struct objectsIn
{
    double **states;
    double **timeSpans;
};

/* function to read input initial conditions files*/
struct objectsIn readObjects(char objectsPath[], int numObjs)
{

    // function definitions
    double** allocateRowArray(int rows, int columns, double *mem);

    char line[MAXLINE];
    int i;
    int j;
    // int numObjs;
    char c;
    int count;
    char delim[] = ",";
    char *str;
    // double **objects;
    // double objects[numObjs]
    double orbels[numObjs][13];
    double buff = 0;
    struct objectsIn objs;
    
    FILE *inFile = fopen(objectsPath, "r");
    
    // test for files not existing.
    if (inFile == NULL)
    {
        printf("Error! Could not open file\n");
        exit(-1); // must include stdlib.h
    }
    
    // count number of lines
    count = 0;
    for (c = getc(inFile); c != EOF; c = getc(inFile))
        if (c == '\n')
            count = count + 1;
    
    fclose(inFile);
    
    // define lines variable
    char lines[count][MAXLINE];
    
    // grab objects
    inFile = fopen(objectsPath, "r");
    
    i = 0;
    while (fgets(line, sizeof(line), inFile)) {
        
        strcpy(lines[i], line);
        i++;
    }
    
    fclose(inFile);
        
    if (debug == 1)
    {
        printf("the input file is '%s'\n", objectsPath);
        
         printf("count = %d\n", count);
        
        printf("numObjs = %d\n", numObjs);
        
        printf("the objects are:\n");
        
        for (i = 0; i <= numObjs-1; i = i + 1)
        {
            printf("%s\n", lines[i]);
        }
        
    }
    
    // parse objects
    printf("parsing objs\n");
    // remove new line character
    for (i = 0; i <= numObjs-1; i = i + 1)
    {
        str = strtok(lines[i], "\n");
        printf("done with %d\n",i);
    }
    printf("parsing objs 2\n");
    for (i = 0; i <= numObjs-1; i = i + 1)
    {
        str = strtok(lines[i], delim);
        j = 0;
        while (str != NULL)
        {
            if (j<12)
            {
                orbels[i][j] = strtof(str, NULL);
                j++;
                
            }
            else
            {
                orbels[i][j] = strtof(str, NULL);
                j = 0;
            }
 
            str = strtok(NULL, delim);
        }
        
    }
    printf("done parsing\n");
    // // store in allocated memory
    // for (i = 0; i<= numObjs-1; i++)
    // {
    //     for (j = 0; j<=12; j++)
    //         objects[i][j] = orbels[i][j];
    // }
    
    //debug commands
    // if (debug == 1)
    // {
    //     for (i = 0; i<= numObjs-1; i++)
    //     {
    //         for (j = 0; j<=12; j++)
    //             printf("%f\n",objects[i][j]);
    //         printf("\n");
    //     }
    // }
    
    // allocate off stack memory for array of orbital elements
    printf("allocating mem\n");
    objs.states = allocateRowArray(numObjs, 12, &buff);
    objs.timeSpans = allocateRowArray(numObjs, 2, &buff);

    // place object states and mjd start/end into appropriate arrays

    for (i = 0; i < numObjs; i++)
    {
        // add orbital elements
        objs.states[i][0] = orbels[i][0];
        objs.states[i][1] = orbels[i][2];
        objs.states[i][2] = orbels[i][3];
        objs.states[i][3] = orbels[i][4];
        objs.states[i][4] = orbels[i][5];
        objs.states[i][5] = orbels[i][6];
        objs.states[i][6] = orbels[i][7];
        objs.states[i][7] = orbels[i][8];
        objs.states[i][8] = orbels[i][9];
        objs.states[i][9] = orbels[i][10];
        objs.states[i][10] = orbels[i][11];
        objs.states[i][11] = orbels[i][12];

        // add time params
        objs.timeSpans[i][0] = orbels[i][0];
        objs.timeSpans[i][1] = orbels[i][1];

    }
    
    return objs;
}

/*function to allocate and array of pointers to rows 1D array (non contiguous)*/
double** allocateRowArray(int rows, int columns, double *mem)
{
    double **arr = (double **)malloc(rows * sizeof(double *));
    (*mem) = rows*sizeof(double *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (double *)malloc(columns * sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr; 
}

double** reallocateRowArray(double **arr, int prevRows, int rows, int columns, double *mem)
{   
    int i;

    arr = realloc(arr, rows*sizeof(double *));
    (*mem) = rows*sizeof(double *);

    // if ( rows < prevRows ) {

    // }
    // for (i = 0; i < prevRows; i++)
    //     free(arr[i]);

    for (int i = 0; i < rows; i++) {
        arr[i] = realloc(arr[i], columns*sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr;
}

/*function to read values from input file*/
struct Input getPropInputs(char inputFile[])
{
    //int debug = 1;
    char line[MAXLINE];
    char lines[54][MAXLINE];
    char inputStr[18][MAXLINE];
    char inputStr2[18][MAXLINE];
    int i;
    int numIn;
    double conv;
    char delim[] = " = ";
    char delim2[] = "\0";
    char *str;
    char *testStr;
    char test[MAXLINE];
    struct Input inputs;
    
    FILE *inFile = fopen(inputFile, "r");
    
    if (debug == 1)
    {
        printf("the input file is '%s'\n", inputFile);
        
    }
    
    // test for files not existing.
    if (inFile == NULL)
    {
        printf("Error! Could not open file\n");
        exit(-1); // must include stdlib.h
    }
    
    i = 0;
    while (fgets(line, sizeof(line), inFile)) {

        strcpy(lines[i], line);
        i++;
    }
    
    fclose(inFile);
    
    strcpy(inputStr[0], lines[1]);   //outputDir - 2
    strcpy(inputStr[1], lines[6]);   //objects - 7
    strcpy(inputStr[2], lines[7]);   //numObjs - 8
    strcpy(inputStr[3], lines[8]);   //numIntervals - 9
    strcpy(inputStr[4], lines[22]);   //insgrav - 23
    strcpy(inputStr[5], lines[23]);  //isun - 24
    strcpy(inputStr[6], lines[24]);  //imoon - 25
    strcpy(inputStr[7], lines[25]);  //idrag - 26
    strcpy(inputStr[8], lines[26]);  //iF107 - 27
    strcpy(inputStr[9], lines[27]);  //iSRP - 28
    strcpy(inputStr[10], lines[28]); //iephem - 29
    strcpy(inputStr[11], lines[29]); //gdeg - 30
    strcpy(inputStr[12], lines[30]); //gord - 31
    strcpy(inputStr[13], lines[39]); //tol - 40
    strcpy(inputStr[14], lines[40]); //tspan - 41
    strcpy(inputStr[15], lines[41]); //tstep - 42
    strcpy(inputStr[16], lines[42]); //imcoll - 43
    strcpy(inputStr[17], lines[48]); //eqs - 49
    
    numIn = sizeof(inputStr)/sizeof(inputStr[0]) - 1;
    
    for (i = 0; i <= numIn; i = i + 1)
    {
        testStr = strtok(inputStr[i], delim);
        while (testStr != NULL)
        {
            strcpy(inputStr2[i], testStr);
            testStr = strtok(NULL, delim);
        }
        str = strtok(inputStr2[i], delim);
    }
    
    for (i = 0; i <= numIn; i = i + 1)
    {
        testStr = strtok(inputStr2[i], "\n");
    }
    
    // output directory
    strcpy(inputs.outputDir, inputStr2[0]);

    // prop objects
    strcpy(inputs.objects, inputStr2[1]);

    // number of prop objects
    conv = strtod(inputStr2[2], NULL);
    inputs.numObjs = conv;

    // number of intervals
    conv = strtod(inputStr2[3], NULL);
    inputs.numIntervals = conv;

    // insgrav
    conv = strtod(inputStr2[4], NULL);
    inputs.insgrav = conv;

    // isun
    conv = strtod(inputStr2[5], NULL);
    inputs.isun = conv;

    // imoon
    conv = strtod(inputStr2[6], NULL);
    inputs.imoon = conv;

    // idrag
    conv = strtod(inputStr2[7], NULL);
    inputs.idrag = conv;

    // iF107
    conv = strtod(inputStr2[8], NULL);
    inputs.iF107 = conv;

    // iSRP
    conv = strtod(inputStr2[9], NULL);
    inputs.iSRP = conv;

    // iephem
    conv = strtod(inputStr2[10], NULL);
    inputs.iephem = conv;

    // gdeg
    conv = strtod(inputStr2[11], NULL);
    inputs.gdeg = conv;

    // gord
    conv = strtod(inputStr2[12], NULL);
    inputs.gord = conv;

    // tol
    inputs.tol = strtod(inputStr2[13], NULL);

    // tspan
    inputs.tspan = strtod(inputStr2[14], NULL);

    // tstep
    inputs.tstep = strtod(inputStr2[15], NULL);

    // imcoll
    conv = strtod(inputStr2[16], NULL);
    inputs.imcoll = conv;

    // eqs
    conv = strtod(inputStr2[17], NULL);
    inputs.eqs = conv;


    if (debug == 6)
    {
        for (i = 0; i <= numIn; i = i + 1)
        {
            printf("%s\n", inputStr2[i]);
        }
        printf("---------------------\n");
        printf("%s\n", inputs.outputDir);
        printf("%s\n", inputs.objects);
        printf("%d\n", inputs.numObjs);
        printf("%d\n", inputs.insgrav);
        printf("%d\n", inputs.isun);
        printf("%d\n", inputs.imoon);
        printf("%d\n", inputs.idrag);
        printf("%d\n", inputs.iF107);
        printf("%d\n", inputs.iSRP);
        printf("%d\n", inputs.iephem);
        printf("%d\n", inputs.gdeg);
        printf("%d\n", inputs.gord);
        printf("%10.10f\n", inputs.tol);
        printf("%10.10f\n", inputs.tspan);
        printf("%10.10f\n", inputs.tstep);
        printf("%d\n", inputs.imcoll);
        printf("%d\n", inputs.eqs);

    }
    
    return inputs;
}

/* allocate memory for 2D contiguous array of ints */
int** allocateInt2DArray(int rows, int columns, double *mem)
{
    int **arr = (int **)malloc(rows * sizeof(int *));
    (*mem) = rows*sizeof(int *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (int *)malloc(columns * sizeof(int));
        (*mem) += columns*sizeof(int);
    }
    
    return arr; 
}

int** reallocateInt2DArray(int **arr, int prevRows, int rows, int columns, double *mem)
{
    int i;

    arr = (int **)realloc(arr, rows * sizeof(int *));
    (*mem) = rows*sizeof(int *);

    for (i = 0; i < prevRows; i++) {
        free(arr[i]);
    }

    for (int i=0; i < rows; i++) {
        arr[i] = (int *)malloc(columns * sizeof(int));
        (*mem) += columns*sizeof(int);
    }

    return arr;
}

/* free 2d int array */
int free2dInt(int **arr, int m)
{
    int i;
    for (i = 0; i < m; i ++)
        free(arr[i]);
    free(arr);
    return 0;
}

int printTrajectory(int npts, int objDesig, double **traj)
{
    int i, j, k;

    for (i = 0; i < npts; i++)
    {
        printf("%.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", traj[objDesig][i * 12 + 0],
                traj[objDesig][i * 12 + 1], traj[objDesig][i * 12 + 2],traj[objDesig][i * 12 + 3],traj[objDesig][i * 12 + 4],
                traj[objDesig][i * 12 + 5], traj[objDesig][i * 12 + 6],traj[objDesig][i * 12 + 7],traj[objDesig][i * 12 + 8],
                traj[objDesig][i * 12 + 9], traj[objDesig][i * 12 + 10],traj[objDesig][i * 12 + 11]);
    }
    return 0;
}

int printStates(int nObjs, double **states)
{
    int i, j, k;

    for ( j = 0; j < nObjs; j++)
    {
    printf("%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    states[j][0], states[j][1], states[j][2], states[j][3],
                    states[j][4], states[j][5], states[j][6], states[j][7],
                    states[j][8], states[j][9], states[j][10], states[j][11]);
    }
    return 0;
}

void calcTspans(int nObjs, double **objects, double *partialTspan, int *mxpts, int numIntervals, double tstep, int *npts)
{
    double locTspan;
    double mjdStart;
    double mjdEnd;
    double val;
    int temp;
    int i;

    for (i = 0; i < nObjs; i++)
    {

        // add to partials array
        mjdStart = objects[i][0];
        mjdEnd = objects[i][1];
        locTspan = mjdEnd - mjdStart;
        partialTspan[i] = locTspan/numIntervals;
        if (partialTspan[i] < 0)
            val = -partialTspan[i];
        else
            val = partialTspan[i];
        
        temp = calcMxpts(val, tstep);
        mxpts[i] = temp;
        npts[i] = temp;

        // free input array
        free(objects[i]);


    }

    // free input array
    free(objects);
}
