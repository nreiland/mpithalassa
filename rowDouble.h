// header file for fica memory management

struct rowDouble
{
    int *rows;
    int *columns;
    double **arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 3d semi contiguous array structure*/
 void allocRowDouble(struct rowDouble *array, int newrows, int newcolumns) {

    int i;

    // allocate mem for organizational pointers
    array->rows = malloc(sizeof(int));
    array->columns = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = calloc(1,sizeof(double));

    // define pointer values for organization pointers
    *(array->rows) = newrows;
    *(array->columns) = newcolumns;

    // allocate memory for main array
    array->arr = (double **)malloc(newrows * sizeof(double *));
    for (i = 0; i < newrows; i++) {
        array->arr[i] = (double *)malloc(newcolumns*sizeof(double));
        memset(array->arr[i], 0, newcolumns*sizeof(double));
    }

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += newrows*sizeof(double *);
    *(array->mem) += sizeof(double **);
    *(array->mem) += newrows*newcolumns*sizeof(double);
    *(array->mem) = *(array->mem)/1e6;
    *(array->prevmem) = 0;

}

int reallocRowDouble(struct rowDouble *array, int newrows)
{
    // define new vars
    int i = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevrows = *(array->rows);

    // define factor of safety
    double fs = 1.0;
    if ( newrows > prevrows )
        newrows = fs*newrows;

    // flag = 1: new rows, flag = 2: prevrows
    int flag = 0; 
    
    // reallocate double pointer if number of rows has increased -> use newrows
    if ( newrows > prevrows ) {

        // printf(" reallocating memory address: %p\n", array->arr );
        // printf("prevrows = %d - - -  newrows = %d\n", prevrows , newrows );

        // reallocate double pointer
        array->arr = realloc( array->arr, newrows*sizeof(double *) );

        // reallocate old rows
        // for (i = 0; i < prevrows; i++) {
        //     array->arr[i] = realloc( array->arr[i], columns*sizeof(double) );
        //     memset( array->arr[i], 0, columns*sizeof(double) );
        // }
        // allocate new rows
        for (i = prevrows; i < newrows; i++) {
            array->arr[i] = (double *)malloc( columns*sizeof(double) );
            // memset( array->arr[i], 0, columns*sizeof(double) );
        }

        flag = 1;
    }
    // if rows have not increased check if number of rows has increased -> use prevl
    else {

        // // set values to zeros
        // for (i = 0; i < newrows; i++) {
        //     memset( array->arr[i], 0, columns*sizeof(double) );
        // }

        flag = 2;

    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += newrows*sizeof(double *);
        newmem += sizeof(double **);
        newmem += newrows*columns*sizeof(double);

        *(array->rows) = newrows;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += 2*sizeof(double *);
        newmem += prevrows*sizeof(double *);
        newmem += sizeof(double **);
        newmem += prevrows*columns*sizeof(double);

        *(array->rows) = prevrows;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem/1e6;

    // return int
    return 0;

}
   