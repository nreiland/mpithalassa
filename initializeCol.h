#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

struct jobParams
{
    int rootNumPropJobs;
    int nomNumPropJobs;
    int propRemainder;
    
};

/* function to calculate maximum Keplerian Velocity */
double calcVmax(double **targetStates, double **fieldStates, int numTargetObjs, int numFieldObjs, int endo)
{
    int i, j;
    double v, a, e, rp, vmax;
    double mu = 3.986004414498200E+05;

    // calculate maximum Keplerian velocity
    a = targetStates[0][1];
    e = targetStates[0][2];
    rp = a*(1 - e*e);
    vmax = sqrt(mu*(2/rp - 1/a));
    for (i = 1; i < numTargetObjs; i++)
    {
        a = targetStates[i][1];
        e = targetStates[i][2];
        rp = a*(1 - e*e);
        v = sqrt(mu*(2/rp - 1/a));

        if (v < vmax)
            vmax = v;
    }
    if (endo == 0)
    {
        for (i = 0; i < numFieldObjs; i++)
        {
            a = fieldStates[i][1];
            e = fieldStates[i][2];
            rp = a*(1 - e*e);
            v = sqrt(mu*(2/rp - 1/a));

            if (v < vmax)
                vmax = v;
        }
    }
    else if (endo != 1)
    {
        printf("please enter a valid value for ''endogenous'' \n");
        exit(-1);
    }

    return 1.5*vmax;
    
}

/* calculate length of propagation jobs and trajectory comparison jobs for exogenous case */
struct jobParams calcJobLengths(int numCores, int numObjs)
{
    int remainder;
    struct jobParams jobs;

    // don't give jobs to root rank
    numCores = numCores - 1;
  
    // calculate number of propagation jobs for target objects
    remainder = numObjs % numCores;
    jobs.nomNumPropJobs = (numObjs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumPropJobs = jobs.nomNumPropJobs;
    else
        jobs.rootNumPropJobs = jobs.nomNumPropJobs + 1;
    jobs.propRemainder = remainder;

    return jobs;
    
}

/* function to populate propagation jobs array */
int initPropArr(int *propJobs, int numObjs)
{
    int i;

    for (i = 0; i < numObjs; i++)
        propJobs[i] = i;
    
    return 0;
}

/* function to populate comparison jobs array */
int initTrajCompArr(int *trajCompJobs, int length)
{
    int i;

    for (i = 0; i < length; i++)
        trajCompJobs[i] = i;
    
    return 0;
}

/* calculate maximum number of points for integrations */
int calcMxpts(double partialTspan, double tstep)
{
    int mxpts, tratio;
    int remainder;
    //double partialTspan;

    tratio = partialTspan/tstep;
    // mxpts = tratio + 100000;
    mxpts = tratio + 5000;

    return mxpts;
    
}

/* function to print trajectory length */
int printTrajLen(int *trajLength, int numObjs)
{
    int i;

    for (i = 0; i < numObjs; i ++)
        printf("%d\n",trajLength[i]);
    
    return 0;
}

/* function to print C array */
int printC(int *C, int combs)
{
    int i;

    for (i = 0; i < combs; i ++)
        printf("%d %d %d %d %d\n", C[i * 5 + 0], C[i * 5 + 1], C[i * 5 + 2],
               C[i * 5 + 3], C[i * 5 + 4]);
    
    return 0;
}

/* function to print P array */
int printP(int **P, int combs)
{
    int i;

    for (i = 0; i < combs; i ++)
        printf("%d %d\n",P[i][0], P[i][1]);
    
    return 0;
}

/* function to free 3d semi contiguous array */
int free3dSemiArray(double **arr, int m)
{
    int i;
    for (i = 0; i < m; i++)
    {
        free(arr[i]);
    }
    free(arr);

    return 0;
}

/* function to initialize mapping */
int initMapping(int *map, int numObjs)
{
    int i;
    for ( i = 0; i < numObjs; i++)
        map[i] = i;
    return 0;
}




