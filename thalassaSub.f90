subroutine thalassasub(MJD0, COE0, tspan, tstep, insgrav, isun, imoon, idrag, &
                       iF107, iSRP, iephem, gdeg, gord, rmxstep, tol, imcoll, &
                       eqs, SCMass, ADrag, ASRP, CD, CR, mxpts, npts, cart_target, &
                       tag, exitcode)

! MODULES
USE ISO_C_BINDING
use KINDS,       only: dk, structOut
use CART_COE,    only: COE2CART,CART2COE
use PHYS_CONST,  only: READ_PHYS, GMST_UNIFORM
use NSGRAV,      only: INITIALIZE_NSGRAV
use PROPAGATE,   only: DPROP_REGULAR
use SUN_MOON,    only: INITIALIZE_LEGENDRE
use IO,          only: READ_IC,id_cart,id_orb,id_stat,id_log,object_path
!use SETTINGS,    only: gdeg,gord,isun,imoon,tol,eqs
use SETTINGS,    only: READ_SETTINGS
use PHYS_CONST,  only: GE,d2r,r2d,secsPerDay,secsPerSidDay,twopi
use SUN_MOON,    only: GslSun,GslMoon
use AUXILIARIES, only: set_mjd

implicit none

!type(structOut) thalassasub

! VARIABLES
! Initial conditions (EMEJ2000)
real(dk)  ::  COE0(1:6),COE0_rad(1:6)
real(dk)  ::  R0(1:3),V0(1:3)
real(dk)  ::  GMST0,aGEO
real(dk)  ::  period
! Integration span and dt [solar days]
!real(dk),intent(in)  ::  tspan,tstep
double precision,intent(in)  ::  tspan,tstep
! Trajectory
integer, intent(out)          ::  npts
integer, intent(in) :: mxpts, tag
integer                       ::  ipt, start, finish
!real(dk),pointer              ::  cart_point(:,:),orb_point(:,:)
!real(dk),pointer,intent(out)  ::  cart_arr, orb_arr
!real(dk),allocatable          ::  cart(:,:),orb(:,:)
!real(dk),target               ::  cart_targ(:,:),orb_targ(:,:)
!real(dk),intent(out) ::  cart(mxpts, 7), orb(mxpts, 7)
TYPE(structOut)   :: output
real(dk)                      ::  R(1:3),V(1:3)
real(dk)                      ::  lifetime_yrs, A2M_DRAG, A2M_SRP
real(dk),intent(in)                      ::  SCMass, ADrag, ASRP, CD, CR
! Measurement of CPU time, diagnostics
integer  ::  rate,tic,toc
real(dk) ::  cputime
integer, intent(out)  ::  exitcode
! Function calls and integration steps
integer  ::  int_steps,tot_calls
! Command arguments
integer  ::  command_arguments
! Date & time
character(len=8)   :: date_start, date_end
character(len=10)  :: time_start, time_end
character(len=5)   :: zone
! Paths
character(len=512) :: earth_path,phys_path
! READ_IC
!real(dk),intent(in)  ::  MJD0
double precision,intent(in)  ::  MJD0
! READ_SETTINGS
! Physical model
integer,intent(in)  ::  insgrav            ! Non-spherical gravity field flag.
integer,intent(in)  ::  isun               ! 0 = no Sun perturbation, >1 otherwise.
integer,intent(in)  ::  imoon              ! 0 = no Moon perturbation, >1 otherwise.
integer,intent(in)  ::  idrag              ! 0 = no atmospheric drag, 1 = Wertz model, 2 = US76 (PATRIUS), 3 = J77 (Carrara - INPE), 4 = NRLMSISE-00 (Picone - NRL)
integer,intent(in)  ::  iF107              ! 0 = constant F10.7 flux, 1 = variable F10.7 flux
integer,intent(in)  ::  iSRP               ! 0 = no SRP, 1 = otherwise.
integer,intent(in)  ::  iephem             ! Ephemerides source. 1 = DE431 ephemerides. 2 = Simpl. Meeus & Brown
integer,intent(in)  ::  gdeg,gord          ! Gravitational potential - maximum degree and order
integer  ::  Mord,Sord          ! Order of the Legendre expansion for the Moon and the Sun
! Integrator settings
!integer             ::  mxstep             ! Max. number of integration/output steps.
integer,intent(in)  ::  rmxstep             ! Max. number of integration/output steps.
real(dk),intent(in) ::  tol                ! Integrator tolerance.
integer,intent(in)  ::  imcoll             ! 0 = do not check for collisions with the Moon, 1 = otherwise.
! Equations of motion settings
integer,intent(in)  ::  eqs                ! Equations of motion type. 1 = Cowell,
! 2 = EDromo(t), 3 = EDromo(c), 4 = EDromo(l)
! OUTPUTS
!real(dk), intent(out) :: cart_target(mxpts,7), orb_target(mxpts,7)
real(dk) :: cart_target(mxpts*12)


! intialize arrays
!orb = 0._dk
!cart = 0._dk

! compute area to mass ratios
A2M_Drag = ADrag/SCMass
A2M_SRP  = ASRP/SCmass

! paths
earth_path = './data/earth_potential/GRIM5-S1.txt'
phys_path = './data/physical_constants.txt'

!write(*,*) "reached this point3"

! Read initial conditions, settings and physical model data.
!write(*,*)"rmxstep:",rmxstep
!write(*,*)insgrav,isun,imoon,idrag,iF107,iSRP,iephem,gdeg,gord,tol,rmxstep,imcoll,eqs
! write(*,*)'Inputs'
! write(*,*)MJD0, COE0, tspan, tstep, mxpts
! write(*,*)tspan
! write(*,*)tstep
call set_mjd(MJD0)
call READ_SETTINGS(insgrav,isun,imoon,idrag,iF107,iSRP,iephem,gdeg,gord,tol,rmxstep,imcoll,eqs)
call READ_IC(A2M_Drag,A2M_SRP,CD,CR)
call READ_PHYS(phys_path)

! Initialize Earth data
call INITIALIZE_NSGRAV(earth_path)

! Initialize Legendre coefficients, if needed
if (isun > 1) then
call INITIALIZE_LEGENDRE(isun,GslSun)

end if
if (imoon > 1) then
call INITIALIZE_LEGENDRE(imoon,GslMoon)

end if

! Load SPICE kernels
call FURNSH('./data/kernels_to_load.furnsh')

! Convert to Cartesian coordinates
COE0_rad = [COE0(1:2),COE0(3:6)*real(d2r,dk)]
call COE2CART(COE0_rad,R0,V0,GE)

! Output to user (not sure what exactly this does)
GMST0 = GMST_UNIFORM(MJD0)
aGEO  = (GE*(secsPerSidDay/twopi)**2)**(1._dk/3._dk)
period = twopi*sqrt(COE0(1)**3/GE)/secsPerSidDay

call DPROP_REGULAR(R0,V0,MJD0,tspan,tstep,tol,eqs,cart_target,int_steps,tot_calls,exitcode,mxpts,npts)

! unload Spice kernels
call UNLOAD('./data/kernels_to_load.furnsh')

end subroutine thalassasub

