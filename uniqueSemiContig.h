// header file for fica memory management

struct uniqueDoubleSemi
{
    int *l;
    int *rows;
    int *columns;
    double **arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 3d semi contiguous array structure*/
 void alloc3DUniqueDoubleSemi(struct uniqueDoubleSemi *array, int newl, int *newrows, int newcolumns) {

    int i;
    double buff = 0;

    // allocate mem for organizational pointers
    array->l = malloc(sizeof(int));
    array->rows = malloc(sizeof(int)*newl);
    array->columns = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double));

    // define pointer values for organizational pointers
    *(array->l) = newl;
    array->rows = newrows;
    *(array->columns) = newcolumns;

    // allocate memory for main array
    array->arr = (double **)malloc(newl * sizeof(double *));
    for (i = 0; i < newl; i++) {
        array->arr[i] = (double *)malloc(newrows[i]*newcolumns*sizeof(double));
        memset(array->arr[i], 0, newrows[i]*newcolumns*sizeof(double));
    }


    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += sizeof(int)*newl;
    *(array->mem) += newl*sizeof(double *);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) += sizeof(double **);
    for (i = 0; i < newl; i++)
    {
        buff += newrows[i]*newcolumns*sizeof(double)/1e6;
    }
    *(array->mem) = *(array->mem)/1e6 + buff;
    *(array->prevmem) = 0;

}

/* function to reallocate 3d semi contiguous array structure*/
int reallocUniqueDouble3Dsemi(struct uniqueDoubleSemi *array, int newl, int *newrows)
{
    // define new vars
    int i = 0;
    int j = 0;
    double newmem = 0;
    double prevmem = *(array->mem);
    int columns = *(array->columns);
    int prevl = *(array->l);
    int *prevrows = array->rows;
    double buff;

    // flag = 1: newl & newrows, flag = 2: newl * prevrows, flag = 3: prevl & newrows, flag = 4: prevl & prevrows
    int flag = 0; 
    
    // reallocate double pointer if l has increased -> use newl
    if ( newl > prevl ) 
    {

        // reallocate double pointer
        array->arr = realloc( array->arr, newl*sizeof(double *) );
        newmem += newl*sizeof(double *)/1e6;

        // loop through old objects
        for (i = 0; i < prevl; i++)
        {
            // reallocate pointers if number of rows has increased -> use newrows
            if ( newrows[i] > prevrows[i] ) 
            {
                // reallocate old pointers
                array->arr[i] = realloc( array->arr[i], newrows[i]*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows[i]*columns*sizeof(double) );
                newmem += newrows[i]*columns*sizeof(double)/1e6;
               
            }
            else
            {
                // zero old array
                memset( array->arr[i], 0, prevrows[i]*columns*sizeof(double) );
                newmem += prevrows[i]*columns*sizeof(double)/1e6;
            }

        }

        // loop through new objects
        for (i = prevl; i < newl; i ++)
        {
            // reallocate pointers if number of rows has increased -> use newrows
            if ( newrows[i] > prevrows[i] ) 
            {
                // reallocate old pointers
                array->arr[i] = realloc( array->arr[i], newrows[i]*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows[i]*columns*sizeof(double) );
                newmem += newrows[i]*columns*sizeof(double)/1e6;
               
            }
            else
            {
                // zero old array
                memset( array->arr[i], 0, prevrows[i]*columns*sizeof(double) );
                newmem += prevrows[i]*columns*sizeof(double)/1e6;
            }
        }
    }
    // if l has not increased check if number of rows has increased -> use prevl
    else 
    {

        // count mem of double pointer
        newmem += prevl*sizeof(double *);

        // loop through old objects
        for (i = 0; i < prevl; i++)
        {
            // reallocate pointers if number of rows has increased -> use newrows
            if ( newrows[i] > prevrows[i] ) 
            {
                // reallocate old pointers
                array->arr[i] = realloc( array->arr[i], newrows[i]*columns*sizeof(double) );
                memset( array->arr[i], 0, newrows[i]*columns*sizeof(double) );
                newmem += newrows[i]*columns*sizeof(double)/1e6;
            } 
            else
            {
                // zero old pointer
                memset( array->arr[i], 0, prevrows[i]*columns*sizeof(double) );
                newmem += prevrows[i]*columns*sizeof(double)/1e6;
            }
            

        }

        

    }

    
    return 0;

}
   